/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

var AOS = require("aos");
AOS.init();

window.Axios = require("axios");

import VueApexCharts from "vue-apexcharts";
Vue.use(VueApexCharts);

Vue.component("apexchart", VueApexCharts);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component("navbar", require("./components/Navbar.vue"));
Vue.component("scroller", require("./components/Scroller.vue"));
Vue.component("flexer", require("./components/Flexer.vue"));
Vue.component("movie", require("./components/Movie.vue"));
Vue.component("genreLinks", require("./components/GenreLinks.vue"));
Vue.component("cart", require("./components/Cart.vue"));
Vue.component("buy", require("./components/Buy.vue"));
Vue.component("orders", require("./components/Orders.vue"));
Vue.component("adminNavbar", require("./components/AdminNavbar.vue"));
Vue.component("adminSidenav", require("./components/AdminSidenav.vue"));
Vue.component("adminMovie", require("./components/AdminMovie.vue"));
Vue.component("adminOrders", require("./components/AdminOrders.vue"));
Vue.component("adminSales", require("./components/AdminSales.vue"));
Vue.component("stripePay", require("./components/StripePay.vue"));

// const files = require.context('./', true, /\.vue$/i)

// files.keys().map(key => {
//     return Vue.component(_.last(key.split('/')).split('.')[0], files(key))
// })

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: "#app"
});
