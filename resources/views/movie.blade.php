@extends('layout.base')
@section('content')
<movie :movie='{{json_encode($movie)}}' :user="{{json_encode($user)}}"></movie>
@endsection