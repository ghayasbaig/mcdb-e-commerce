<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href={{url('css/app.css')}}>
    <link rel="stylesheet" href={{url('css/aos.css')}}>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <title>Movie Chronicle Database</title>
    <style>
        body{
            position: relative;
            background-color: black;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }
        .main-content{
            position: relative;
            top: 115px;
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: flex-start;
            align-items: flex-start;
        }
        html{
            width: 100%;
            height: 100%;
            scroll-behavior: smooth;
        }
        #app{
            position: relative;
            width: 100%;
            height: 100%;
            display: : none
        }
        .footer{
            position: sticky;
            position: -webkit-sticky;
            top: calc(100% - 80px);
            height: 80px;
            width: 100%;
            background-color: black;
            text-align: center;
        }
        .footerText{
            color: white
        }
    </style>
</head>
<body>
    <div id="app">
        <admin-navbar :brand="{name: '', link: '/brand.jpg'}" :user="{{json_encode($user)}}"></admin-navbar>
        <div class="main-content">
        <admin-sidenav style="flex: 0.2" linkname=<?php echo($linkName);?>></admin-sidenav>
        <div style="flex: 0.8">
            @yield('content')
        </div>
        </div>
        <!--<footer class="footer">
              <p class="footerText"><span class="footerDate"></span> © All Rights Reserved</p><a href="/" style="color: red">Movie Chronicle Database</a>
          </footer>-->
    </div> 
    <div id="loading" style="width: 100%; height: 100%; display: flex; justify-content:center; align-items:center">
            <div id="loadinganim" style="height: 100px; width: 100px"></div>
            </div>
</body>

<script src={{asset('js/app.js')}}></script>
<script src={{asset('js/lottie.js')}}></script>
<script>
    lottie.loadAnimation({
    container: document.getElementById("loadinganim"), // the dom element that will contain the animation
    renderer: 'svg',
    loop: true,
    autoplay: true,
    animationData: {"v":"4.6.2","fr":60,"ip":0,"op":47,"w":180,"h":180,"nm":"1","ddd":0,"assets":[],"layers":[{"ddd":0,"ind":1,"ty":4,"nm":"形状图层 3","ks":{"o":{"a":0,"k":100},"r":{"a":0,"k":0},"p":{"a":1,"k":[{"i":{"x":0.4,"y":1},"o":{"x":0.3,"y":0},"n":"0p4_1_0p3_0","t":2.607,"s":[149.516,86.766,0],"e":[92.516,86.766,0],"to":[-9.5,0,0],"ti":[9.5,0,0]},{"t":43}]},"a":{"a":0,"k":[-49.984,-3.234,0]},"s":{"a":1,"k":[{"i":{"x":[0.833,0.833,0.833],"y":[1,1,0.833]},"o":{"x":[0.3,0.3,0.3],"y":[0,0,0.3]},"n":["0p833_1_0p3_0","0p833_1_0p3_0","0p833_0p833_0p3_0p3"],"t":3.082,"s":[100,100,100],"e":[130,130,100]},{"i":{"x":[0.833,0.833,0.833],"y":[1,1,0.833]},"o":{"x":[0.167,0.167,0.167],"y":[0,0,0.167]},"n":["0p833_1_0p167_0","0p833_1_0p167_0","0p833_0p833_0p167_0p167"],"t":19.369,"s":[130,130,100],"e":[100,100,100]},{"t":46}]}},"ao":0,"shapes":[{"ty":"gr","it":[{"d":1,"ty":"el","s":{"a":0,"k":[31.032,31.032]},"p":{"a":0,"k":[0,0]},"nm":"椭圆路径 1","mn":"ADBE Vector Shape - Ellipse"},{"ty":"fl","c":{"a":0,"k":[1,0.3176471,0.3176471,1]},"o":{"a":0,"k":100},"r":1,"nm":"填充 1","mn":"ADBE Vector Graphic - Fill"},{"ty":"tr","p":{"a":0,"k":[-49.984,-3.234],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"变换"}],"nm":"椭圆 1","np":3,"cix":2,"ix":1,"mn":"ADBE Vector Group"}],"ip":0,"op":480,"st":0,"bm":0,"sr":1},{"ddd":0,"ind":2,"ty":4,"nm":"形状图层 2","ks":{"o":{"a":0,"k":100},"r":{"a":0,"k":0},"p":{"a":1,"k":[{"i":{"x":0.4,"y":1},"o":{"x":0.3,"y":0},"n":"0p4_1_0p3_0","t":2.607,"s":[92.516,86.766,0],"e":[35.516,86.766,0],"to":[-9.5,0,0],"ti":[9.5,0,0]},{"t":43}]},"a":{"a":0,"k":[-49.984,-3.234,0]},"s":{"a":1,"k":[{"i":{"x":[0.4,0.4,0.4],"y":[1,1,0.4]},"o":{"x":[0.3,0.3,0.3],"y":[0,0,0.3]},"n":["0p4_1_0p3_0","0p4_1_0p3_0","0p4_0p4_0p3_0p3"],"t":3.082,"s":[100,100,100],"e":[130,130,100]},{"i":{"x":[0.833,0.833,0.833],"y":[1,1,0.833]},"o":{"x":[0.3,0.3,0.3],"y":[0,0,0.3]},"n":["0p833_1_0p3_0","0p833_1_0p3_0","0p833_0p833_0p3_0p3"],"t":19.369,"s":[130,130,100],"e":[95,95,100]},{"t":46}]}},"ao":0,"shapes":[{"ty":"gr","it":[{"d":1,"ty":"el","s":{"a":0,"k":[31.032,31.032]},"p":{"a":0,"k":[0,0]},"nm":"椭圆路径 1","mn":"ADBE Vector Shape - Ellipse"},{"ty":"fl","c":{"a":0,"k":[1,0.3176471,0.3176471,1]},"o":{"a":0,"k":100},"r":1,"nm":"填充 1","mn":"ADBE Vector Graphic - Fill"},{"ty":"tr","p":{"a":0,"k":[-49.984,-3.234],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"变换"}],"nm":"椭圆 1","np":3,"cix":2,"ix":1,"mn":"ADBE Vector Group"}],"ip":0,"op":480,"st":0,"bm":0,"sr":1},{"ddd":0,"ind":3,"ty":4,"nm":"形状图层 1","ks":{"o":{"a":0,"k":100},"r":{"a":0,"k":0},"p":{"a":1,"k":[{"i":{"x":0.4,"y":1},"o":{"x":0.3,"y":0.168},"n":"0p4_1_0p3_0p168","t":0,"s":[35.516,86.766,0],"e":[149.766,86.766,0],"to":[19.0416660308838,0,0],"ti":[-19.0416660308838,0,0]},{"t":40.392578125}]},"a":{"a":0,"k":[-49.984,-3.234,0]},"s":{"a":1,"k":[{"i":{"x":[0.4,0.4,0.4],"y":[1,1,0.4]},"o":{"x":[0.3,0.3,0.3],"y":[0,0,0.3]},"n":["0p4_1_0p3_0","0p4_1_0p3_0","0p4_0p4_0p3_0p3"],"t":0,"s":[100,100,100],"e":[80,80,100]},{"i":{"x":[0.4,0.4,0.4],"y":[0.4,0.4,0.4]},"o":{"x":[0.3,0.3,0.3],"y":[0.3,0.3,0.3]},"n":["0p4_0p4_0p3_0p3","0p4_0p4_0p3_0p3","0p4_0p4_0p3_0p3"],"t":7.701,"s":[80,80,100],"e":[80,80,100]},{"i":{"x":[0.4,0.4,0.4],"y":[1,1,0.4]},"o":{"x":[0.3,0.3,0.3],"y":[0,0,0.3]},"n":["0p4_1_0p3_0","0p4_1_0p3_0","0p4_0p4_0p3_0p3"],"t":28.504,"s":[80,80,100],"e":[100,100,100]},{"t":45.927734375}]}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ks":{"a":1,"k":[{"i":{"x":0.4,"y":1},"o":{"x":0.3,"y":0},"n":"0p4_1_0p3_0","t":3.908,"s":[{"i":[[-8.569,0],[0,-8.569],[8.569,0],[0,8.569]],"o":[[8.569,0],[0,8.569],[-8.569,0],[0,-8.569]],"v":[[0,-15.516],[15.516,0],[0,15.516],[-15.516,0]],"c":true}],"e":[{"i":[[-8.569,0],[0,-8.569],[8.569,0],[0,8.569]],"o":[[8.569,0],[0,8.569],[-8.569,0],[0,-8.569]],"v":[[0,-15.516],[15.516,0],[0,15.516],[-22.547,0.129]],"c":true}]},{"i":{"x":0.4,"y":1},"o":{"x":0.3,"y":0},"n":"0p4_1_0p3_0","t":20.328,"s":[{"i":[[-8.569,0],[0,-8.569],[8.569,0],[0,8.569]],"o":[[8.569,0],[0,8.569],[-8.569,0],[0,-8.569]],"v":[[0,-15.516],[15.516,0],[0,15.516],[-22.547,0.129]],"c":true}],"e":[{"i":[[-8.569,0],[0,-8.569],[8.569,0],[0,8.569]],"o":[[8.569,0],[0,8.569],[-8.569,0],[0,-8.569]],"v":[[0,-15.516],[15.516,0],[0,15.516],[-15.983,0.08]],"c":true}]},{"t":32.0546875}]},"nm":"路径 1","mn":"ADBE Vector Shape - Group"},{"ty":"fl","c":{"a":0,"k":[0.8447763,0.2541058,0.2541058,1]},"o":{"a":0,"k":100},"r":1,"nm":"填充 1","mn":"ADBE Vector Graphic - Fill"},{"ty":"tr","p":{"a":0,"k":[-49.984,-3.234],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"变换"}],"nm":"椭圆 1","np":3,"cix":2,"ix":1,"mn":"ADBE Vector Group"}],"ip":0,"op":480,"st":0,"bm":0,"sr":1}]}
  });
    window.onload = () => {
        document.getElementById("app").style.display = "block";
        document.getElementById("loading").style.display = "none";
    }
</script>
<script> var date = new Date().getFullYear(); document.getElementsByClassName("footerDate")[0].innerText = date;</script>
</html>

