@extends('layout.base')
@section('content')
<flexer heading="Generefied" :movies-list='{{json_encode($movies)}}' :user="{{json_encode($user)}}">
<genre-links :links='{{json_encode($links)}}' active-genre='{{$activeGenreID}}'></genre-links></flexer>
<div class="paging">{{$movies->links()}}</div>
@endsection

<style>
        .paging{
            position: fixed;
            bottom: 0;
            justify-content: center;
            display: flex;
            flex-wrap: nowrap;
            flex-direction: row;
            width: 100%;
            z-index: 2 !important;
        }
    
        .pagination{
            background: linear-gradient(to left, rgb(200, 33, 58), rgb(100, 41, 30));
            border-radius: 20px !important;
        }
    
        .page-item, .page-link{
            background: transparent;
            color: red !important; 
        }
    </style>