@extends('layout.base')
@section('content')
<stripe-pay :cart-data='{{json_encode($cartData)}}'></stripe-pay>
@endsection
<script src="https://js.stripe.com/v3/"></script>