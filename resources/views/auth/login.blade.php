@extends('layout.base')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 cardCustContainer">
            <div class="cardCust">
                <div class="card-header">{{ __('Login To Movie Chronicle Database') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6 inputContainerCust">
                                <input placeholder="enter your email..." id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6 inputContainerCust">
                                <input placeholder="enter your password..." id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4 btnContainerCust">
                                <button type="submit" class="btnCust">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                                <a class="btn btn-link link" href="/register">
                                        {{ __('Register?') }}
                                    </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .cardCust{
        background: linear-gradient(to right, rgb(200, 33, 58), rgb(100, 41, 30));
        border-radius: 20px !important;
        color: white;
    }

    .cardCustContainer{
        margin-top: 115px;
    }

    .btnCust{
        background: linear-gradient(to left, rgb(0, 0, 0), rgb(67, 67, 67));
        border-radius: 20px;
        color: white;
        outline: none;
        border: none;
        padding: 5px 10px 5px 10px;
    }
    .btnCust:hover{
        cursor: pointer;
        outline: none;
        border: none;
    }

    #email, #password{
        border-radius: 20px;
    }

    .link{
        color: white !important;
        text-decoration: none
    }
</style>
