@extends('layout.adminBase')
@section('content')
    <admin-orders :processing-orders-prop='{{json_encode($processingOrders)}}' :shipped-orders-prop='{{json_encode($shippedOrders)}}' :delivered-orders-prop='{{json_encode($deliveredOrders)}}' :user='{{json_encode($user)}}'></admin-orders>
@endsection