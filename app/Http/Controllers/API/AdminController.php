<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use stdClass;
use Carbon\Carbon;

use App\movie as Movies;
use App\genre as Genre;
use App\actorMovies;
use App\trailer;
use App\movieGenres;
use App\actor as Actor;
use App\MovieQuality;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'App\Http\Middleware\isAdmin']);
    }
    public function index()
    {
        return view('adminMovie')->with(['user' => auth()->user(), 'linkName' => 'Movies']);
    }

    public function adminOrders()
    {
        return view('adminOrders')->with(['user' => auth()->user(), 'linkName' => 'Orders', 'processingOrders' => Order::where([['status', '=', 1]])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get(), 'shippedOrders' => Order::where([['status', '=', 2]])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get(), 'deliveredOrders' => Order::where([['status', '=', 3]])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get()]);
    }

    public function searchOrders(Request $request)
    {
        $order = new stdClass();
        $order->processing = Order::where([['receiver', 'like', $request->search . '%'], ['status', '=', '1']])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get();
        $order->shipped = Order::where([['receiver', 'like', $request->search . '%'], ['status', '=', '2']])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get();
        $order->delivered = Order::where([['receiver', 'like', $request->search . '%'], ['status', '=', '3']])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->orderBy('updated_at', 'desc')->get();
        return json_encode($order);
    }

    public function deleteOrder(Request $request)
    {
        Order::where('id', '=', $request->orderID)->delete();
        return 1;
    }

    public function orderShipped(Request $request)
    {
        Order::where('id', '=', $request->orderID)->update(['status' => 2]);
        return 1;
    }
    public function orderDelivered(Request $request)
    {
        Order::where('id', '=', $request->orderID)->update(['status' => 3]);
        return 1;
    }

    public function getOrdersGroupedByMonth()
    {
        return view('adminSales')->with(['data' => Order::where([["created_at", ">", date('Y-m-d H:i:s', strtotime("-4 months"))], ["status", "=", "3"]])->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->get()->groupBy(function ($query) {
            return Carbon::parse($query->created_at)->format('F');
        }), 'user' => auth()->user(), 'linkName' => 'Sales']);
    }

    //Edit or Add Movie
    public function saveMovie(Request $request)
    {
        $movie = json_decode($request->movie);
        $deletedActors = json_decode($request->deletedActors);
        $deletedGenres = json_decode($request->deletedGenres);
        $deletedTrailers = json_decode($request->deletedTrailers);

        //Update If Exists
        if (isset($movie->id))
            Movies::where([["id", "=", $movie->id]])->update(["name" => $movie->name, "cover" => $movie->cover, "thumbnail" => $movie->thumbnail, "synopsis" => $movie->synopsis, "releaseDate" => $movie->releaseDate, "length" => $movie->length, "rating" => $movie->rating]);
        else {
            //Create New Movie
            $newMovie = new Movies();
            $newMovie->name = $movie->name;
            $newMovie->cover = $movie->cover;
            $newMovie->thumbnail = $movie->thumbnail;
            $newMovie->synopsis = $movie->synopsis;
            $newMovie->releaseDate = $movie->releaseDate;
            $newMovie->length = $movie->length;
            $newMovie->rating = $movie->rating;
            $newMovie->save();
            $movie->id = $newMovie->id;
        }

        foreach ($movie->genres as $genre) {
            $existingGenre = Genre::where([["name", "=", $genre->name]])->first();
            if (!$existingGenre) {
                $newGenre = new Genre();
                $newGenre->name = $genre->name;
                $newGenre->save();
                $existingGenre = $newGenre;
                $existingGenre->movie_genres_id = $newGenre->id;
            }
            movieGenres::updateOrCreate(["movie_id" => $movie->id, "id" => $existingGenre->movie_genres_id], ["movie_id" => $movie->id, "id" => $existingGenre->movie_genres_id]);
        }

        foreach ($movie->actors as $actor) {
            $existingActor = Actor::where([["name", "=", $actor->name]])->first();

            if (!$existingActor) {
                $newActor = new Actor();
                $newActor->name = $actor->name;
                $newActor->save();
                $existingActor = $newActor;
                $existingActor->actor_movies_id = $newActor->id;
            }
            actorMovies::updateOrCreate(["movie_id" => $movie->id, "id" => $existingActor->actor_movies_id], ["movie_id" => $movie->id, "id" => $existingActor->actor_movies_id]);
        }

        foreach ($movie->trailers as $trailer) {
            trailer::updateOrCreate(["link" => $trailer->link, "movie_id" => $movie->id], ["link" => $trailer->link, "movie_id" => $movie->id]);
        }

        foreach ($deletedActors as $deletedActor) {
            if (isset($deletedActor->actor_movies_id)) //Handle Actors who were added and deleted right away on the UI
                actorMovies::where([["id", "=", $deletedActor->actor_movies_id]])->delete();
        }

        foreach ($deletedGenres as $deletedGenre) {
            if (isset($deletedGenre->movie_genres_id)) //Handle Genres which were added and deleted right away on the UI
                movieGenres::where([["id", "=", $deletedGenre->movie_genres_id]])->delete();
        }

        foreach ($deletedTrailers as $deletedTrailer) {
            if (isset($deletedTrailer->id)) //Handle Trailers which were added and deleted right away on the UI
                trailer::where([["id", "=", $deletedTrailer->id]])->delete();
        }

        //Assuming all movies are intially available in all qualities
        for ($i = 1; $i < 4; $i++) {
            $movieQuality = new MovieQuality();
            $movieQuality->id = $i;
            $movieQuality->movie_id = $movie->id;
            $movieQuality->save();
        }
        return 1;
    }
}
