<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\movie as Movies;
use App\genre as Genre;
use App\comment as Comments;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //Home Page
    public function index()
    {
        return view('index')->with(['movies' => Movies::with(['genres', 'trailers', 'qualities'])->orderBy("releaseDate", "DESC")->simplePaginate(15), 'linkName' => 'Home', 'user' => auth()->user()]);
    }

    //Top Movies
    public function top()
    {
        return view('top')->with(['movies' => Movies::with(['genres', 'qualities', 'trailers' => function ($query) {
            $query->take(3);
        }])->orderBy("rating", "DESC")->simplePaginate(15), 'linkName' => 'Top', 'user' => auth()->user()]);
    }

    //Movies Genre-Wise
    public function genrefy()
    {
        return view('genrefy')->with(['movies' => Movies::with(['genres', 'trailers', 'qualities'])->orderBy("releaseDate", "DESC")->simplePaginate(15), 'links' => $this->getGenreIDs(), 'activeGenreID' => 0, 'linkName' => 'Genrefy', 'user' => auth()->user()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //Movie Page
    public function show($id)
    {
        $movie =  Movies::with(['genres', 'qualities', 'trailers' => function ($query) {
            $query->take(3);
        }, 'actors', 'comments' => function ($query) {
            $query->with("user")->orderBy('id', 'desc');
        }])->find($id);
        return view('movie')->with(['movie' => $movie, 'linkName' => null, 'user' => auth()->user()]);
    }

    //Delete a Movie
    public function deleteMovie(Request $request)
    {
        Movies::where("id", "=", $request->movie_id)->delete();
        return 1;
    }

    //Movies with same Genres
    public function similarGenres($genreids)
    {
        echo Movies::with(['genres', 'trailers'])->genreIDs(json_decode($genreids))->get();
    }

    //Movies Ordered by Name
    public function byName()
    {
        echo Movies::with(['genres', 'trailers' => function ($query) {
            $query->take(3);
        }])->orderBy("name")->get();
    }

    //Searched Movies
    public function search(Request $request)
    {
        echo Movies::with(['genres', 'actors', 'qualities', 'trailers' => function ($query) {
            $query->take(3);
        }])->where("name", "like", $request->search . "%")->get();
    }

    //Top Rated Movies
    public function byRating()
    {
        echo Movies::with(['genres', 'trailers' => function ($query) {
            $query->take(3);
        }])->orderBy("rating", "DESC")->get();
    }


    public function byGenre($id)
    {
        return view('genrefy')->with(['movies' => Movies::with(['genres', 'qualities', 'trailers' => function ($query) {
            $query->take(3);
        }])->whereHas("genres", function ($query) use ($id) {
            $query->where("movie_genres.id", "=", $id);
        })->simplePaginate(15), 'links' => $this->getGenreIDs(), 'linkName' => 'Genrefy', 'user' => auth()->user(), 'activeGenreID' => $id]);
    }

    //Get Only TV Shows
    public function tvShows()
    {
        echo Movies::with(['genres', 'trailers'])->where("isTvSeries", "=", "1")->get();
    }


    //Get IDs of all Genres
    public function getGenreIDs()
    {
        return Genre::all();
    }

    //RN, Filters
    public function filter($filters)
    {
        $filters = json_decode($filters, true);
        echo Movies::with(['genres', 'trailers'])->rating($filters[1]["values"])->genreIDs($filters[0]["values"])->moviesOnly($filters[2]["values"], $filters[3]["values"])->tvShowsOnly($filters[3]["values"], $filters[2]["values"])->orderBy("releaseDate", "DESC")->get();
    }

    //Get comments of One Movie
    public function getMovieComments(Request $request)
    {
        echo Comments::with("user")->where("movieID", "=", $request->movieID)->orderBy('id', 'desc')->get();
    }
}
