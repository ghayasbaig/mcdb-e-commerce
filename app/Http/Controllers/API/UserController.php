<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\movie as Movies;
use App\comment as Comments;
use App\Order;
use App\UserCart;
use App\OrderDetail;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    //RN, Upload Profile Pic
    public function uploadProfilePic(Request $request)
    {
        if (!file_exists(public_path("pps/" . $request->userID))) {
            mkdir(public_path("pps/" . $request->userID), 0777, true);
        }

        $file = $request->file("profilePic");
        if ($file->move(public_path("pps/" . $request->userID), $file->getClientOriginalName() . ".jpg")) {
            User::where("id", "=", $request->userID)->update(["profilePic" => "pps/" . $request->userID . "/" . $file->getClientOriginalName() . ".jpg"]);
        } else echo 0;
    }

    //RN, Get Profile Pic
    public function getPP(Request $request)
    {
        $user = User::where("id", "=", $request->userID)->get();
        echo $user[0]->profilePic;
    }

    //RN, Get Favorite Movies
    public function getFav(Request $request)
    {
        $movies = Favorites::where("user", "=", $request->userID)->get();
        $movieIDs = [];
        foreach ($movies as $movie)
            array_push($movieIDs, $movie["movie"]);

        $movies = Movies::with(['genres', 'trailers' => function ($query) {
            $query->take(3);
        }])->whereIn("id", $movieIDs)->get();
        echo json_encode(["movies" => $movies, "movieIDs" => $movieIDs]);
    }

    //RN, Get isFavorite
    public function getIsFav(Request $request)
    {
        $fav = Favorites::where("movie", "=", $request->movieID)->where("user", "=", $request->userID)->get();
        if (count($fav))
            echo 1;
        else
            echo 0;
    }

    //RN, Add To Favorites
    public function addMyFav(Request $request)
    {
        $fav = new Favorites();
        $fav->movie = $request->movieID;
        $fav->user = $request->userID;
        $fav->save();
    }

    //RN, Remove From Favorites
    public function remMyFav(Request $request)
    {
        if (Favorites::where("user", "=", $request->userID)->where("movie", "=", $request->movieID)->delete())
            echo 1;
        else
            echo 0;
    }

    //Get Items In Cart
    public function cartItems()
    {
        return UserCart::where('user_id', '=', auth()->user()->id)->with(['movie', 'quality'])->get();
    }

    //Add Movie To Cart
    public function addToCart(Request $request)
    {
        $cartItem = new UserCart();
        $cartItem->user_id = auth()->user()->id;
        $cartItem->movie_id = $request->movie_id;
        $cartItem->quality_id = $request->quality_id;
        $cartItem->created_at = date('Y-m-d H:i:s');
        $cartItem->updated_at = date('Y-m-d H:i:s');

        if ($cartItem->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    //Remove Movie From Cart
    public function removeFromCart(Request $request)
    {
        if (UserCart::where([['user_id', '=', auth()->user()->id], ['movie_id', '=', $request->movie_id], ['quality_id', '=', $request->quality_id]])->delete()) {
            return 1;
        } else {
            return 0;
        }
    }

    //Get All Orders
    public function orders(Request $request)
    {
        return view('orders')->with(['orders' => Order::where('user_id', '=', auth()->user()->id)->with(['orderStatus', 'orderDetails' => function ($query) {
            $query->with(['movie', 'quality'])->get();
        }])->get(), 'user' => auth()->user(), 'linkName' => 'Orders']);
    }

    //Payment Page
    public function payWithStripe(Request $request)
    {
        $cartData = UserCart::where('user_id', '=', auth()->user()->id)->with(['movie', 'quality'])->get();
        if (count($cartData))
            return view('stripePay')->with(['cartData' => $cartData, 'linkName' => 'Stripe', 'user' => auth()->user()]);

        return redirect("/");
    }

    //Finalize Payment
    public function checkout(Request $request)
    {
        $order = new Order();
        $order->user_id = auth()->user()->id;
        $order->status = 1;
        $order->receiver = auth()->user()->name;
        $order->created_at = date('Y-m-d H:i:s');
        $order->updated_at = date('Y-m-d H:i:s');
        $order->save();

        $orders = UserCart::where([['user_id', '=', auth()->user()->id]])->with(['movie', 'quality'])->get();
        $price = 0;
        foreach ($orders as $orDe) {
            $orderDetail = new OrderDetail();
            $orderDetail->order_id = $order->id;
            $orderDetail->movie_id = $orDe->movie_id;
            $orderDetail->quality_id = $orDe->quality_id;
            $orderDetail->created_at = date('Y-m-d H:i:s');
            $orderDetail->updated_at = date('Y-m-d H:i:s');
            $orderDetail->save();

            UserCart::where([['user_id', '=', auth()->user()->id], ['movie_id', '=', $orDe->movie_id], ['quality_id', '=', $orDe->quality_id]])->delete();
            $price += $orDe->quality->price;
        }
        try {
            $charge = Stripe::charges()->create([
                'amount' => $price,
                'currency' => 'USD',
                'source' => $request->tokenID,
                'description' => 'Bought Movie(s) from MCDB',
                'receipt_email' => auth()->user()->email,
            ]);

            return 1;
        } catch (CardErrorException $e) {
            return 0;
        }
    }

    //Post New Comment
    public function postCommentWeb(Request $request)
    {
        $comment = new Comments();
        $comment->user_id = auth()->user()->id;
        $comment->movie_id = $request->movieID;
        $comment->text = $request->text;
        $comment->created_at = date('Y-m-d H:i:s');
        $comment->updated_at = date('Y-m-d H:i:s');

        if ($comment->save())
            echo Comments::with("user")->find($comment->id);
        else
            echo 0;
    }

    //RN, Post New Comment
    public function postComment(Request $request)
    {
        $comment = new Comments();
        $comment->user_id = $request->userID;
        $comment->movie_id = $request->movieID;
        $comment->text = $request->text;
        $comment->created_at = date('Y-m-d H:i:s');
        $comment->updated_at = date('Y-m-d H:i:s');

        if ($comment->save())
            echo 1;
        else
            echo 0;
    }
}
