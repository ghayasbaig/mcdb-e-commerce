<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trailer extends Model
{
    public $timestamps = false;
    protected $fillable = ["movie_id", "link"];
}
