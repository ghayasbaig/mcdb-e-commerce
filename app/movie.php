<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class movie extends Model
{
    public $timestamps = false;

    public function genres()
    {
        return $this->hasManyThrough('App\genre', 'App\movieGenres');
    }

    public function trailers()
    {
        return $this->hasMany('App\trailer');
    }

    public function actors()
    {
        return $this->hasManyThrough('App\actor', 'App\actorMovies');
    }

    public function comments()
    {
        return $this->hasMany('App\comment');
    }

    public function qualities()
    {
        return $this->hasManyThrough('App\Quality', 'App\MovieQuality');
    }

    public function scopeGenreIDs($query, $genreIDs)
    {
        if (count($genreIDs))
            return $query->with('genres')->whereHas("genres", function ($query) use ($genreIDs) {
                $query->whereIn("movie_genres.id", $genreIDs);
            });
    }

    public function scopeRating($query, $rating)
    {
        return $query->where("rating", ">=", $rating);
    }

    public function scopeMoviesOnly($query, $moviesOnly, $tvShowsOnly)
    {
        if ($moviesOnly && !$tvShowsOnly)
            return $query->where("isTvSeries", "=", "0");
    }

    public function scopeTvShowsOnly($query, $tvShowsOnly, $moviesOnly)
    {
        if ($tvShowsOnly && !$moviesOnly)
            return $query->where("isTvSeries", "=", "1");
    }
}
