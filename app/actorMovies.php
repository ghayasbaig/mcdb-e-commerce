<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actorMovies extends Model
{
    public $timestamps = false;
    protected $fillable = ["movie_id", "id"];
}
