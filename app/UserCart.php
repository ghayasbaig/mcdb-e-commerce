<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCart extends Model
{
    //
    public function movie()
    {
        return $this->hasOne('App\movie', 'id', 'movie_id');
    }

    public function quality()
    {
        return $this->hasOne('App\Quality', 'movie_quality_id', 'quality_id');
    }
}
