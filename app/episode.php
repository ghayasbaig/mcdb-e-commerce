<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class episode extends Model
{
    public $timestamps = false;

    public function series(){
        return $this->belongsTo('App\tvseries');
    }
}
