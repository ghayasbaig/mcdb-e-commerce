<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class movieGenres extends Model
{
    public $timestamps = false;
    protected $fillable = ["movie_id", "id"];

    public function genre()
    {
        return $this->belongsTo('App\genre');
    }
}
