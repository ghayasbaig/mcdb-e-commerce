<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieQuality extends Model
{
    //
    public $timestamps = false;

    public function quality()
    {
        return $this->hasMany('App\Quality');
    }
}
