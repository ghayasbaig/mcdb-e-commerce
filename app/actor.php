<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class actor extends Model
{
    public $timestamps = false;

    public function movies()
    {
        return $this->hasMany('App\movie');
    }
}
