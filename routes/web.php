<?php

//use Illuminate\Routing\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


//Movie Controller Routes
Route::get('/', "API\MovieController@index");
Route::get('/top', "API\MovieController@top");
Route::get('/genrefy', "API\MovieController@genrefy");
Route::get('/movies/one/{id}', 'API\MovieController@show');
Route::post("/movies/search/", "API\MovieController@search");
Route::get('/movies', 'API\MovieController@index');
Route::get('/tvshows', 'API\MovieController@tvShows');
Route::get('/movies/genre/{id}', 'API\MovieController@byGenre');
Route::get('/movies/rating/top', 'API\MovieController@byRating');
Route::get('/movies/name', 'API\MovieController@byName');
Route::get('/movies/alpha/{alphabet}', 'API\MovieController@byAlphabet');
Route::get('/movies/similar/{genreids}', 'API\MovieController@similarGenres');
Route::get('/movies/filter/{filters}', 'API\MovieController@filter');
Route::get('/genres/all', 'API\MovieController@getGenreIDs');

//Admin Controller Routes
Route::post('/savemovie', 'API\AdminController@saveMovie');
Route::post('/deletemovie', 'API\AdminController@deleteMovie');
Route::get('/admin', "API\AdminController@index");
Route::get('/adminorders', "API\AdminController@adminOrders");
Route::get('/adminsales', "API\AdminController@getOrdersGroupedByMonth");
Route::post('/searchorders', "API\AdminController@searchOrders");
Route::post('/deleteorder', "API\AdminController@deleteOrder");
Route::post('/ordershipped', "API\AdminController@orderShipped");
Route::post('/orderdelivered', "API\AdminController@orderDelivered");

//User Controller Routes
Route::get('/orders', 'API\UserController@orders');
Route::get('/paywithstripe', 'API\UserController@payWithStripe');
Route::post('/checkout', 'API\UserController@checkout');
Route::post('/cartitems', 'API\UserController@cartItems');
Route::post('/addtocart', 'API\UserController@addToCart');
Route::post('/removefromcart', 'API\UserController@removeFromCart');
Route::post('/postcomment', 'API\UserController@postCommentWeb');

//About Contoller Routes
Route::get('/about', "AboutController@index");

//Authorization Routes
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

//Route::post('/placeorder', 'API\RequestController@placeOrder');
