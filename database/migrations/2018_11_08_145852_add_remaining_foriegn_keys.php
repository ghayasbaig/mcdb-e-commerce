<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemainingForiegnKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("comments", function(Blueprint $table){
            $table->foreign("userID")->references("id")->on("users")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("movieID")->references("id")->on("movies")->onDelete("cascade")->onUpdate("cascade");
        });

        Schema::table("episodes", function(Blueprint $table){
            $table->foreign("seriesID")->references("id")->on("tv_series")->onDelete("cascade")->onUpdate("cascade");
        });

        Schema::table("movie_genres", function(Blueprint $table){
            $table->foreign("genreID")->references("id")->on("genres")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("movieID")->references("id")->on("movies")->onDelete("cascade")->onUpdate("cascade");
        });

        Schema::table("ratings", function(Blueprint $table){
            $table->foreign("userID")->references("id")->on("users")->onDelete("cascade")->onUpdate("cascade");
            $table->foreign("movieID")->references("id")->on("movies")->onDelete("cascade")->onUpdate("cascade");
        });

        Schema::table("trailers", function(Blueprint $table){
            $table->foreign("movieID")->references("id")->on("movies")->onDelete("cascade")->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
