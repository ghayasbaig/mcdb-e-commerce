-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2019 at 04:54 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mcdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors`
--

CREATE TABLE `actors` (
  `actor_movies_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actors`
--

INSERT INTO `actors` (`actor_movies_id`, `name`) VALUES
(2, 'Mark Hamill'),
(3, 'Harrison Ford'),
(4, 'Carrie Fisher'),
(6, 'Anthony Daniels'),
(32, 'Robin Wright'),
(34, 'Mykelti Williamson'),
(85, 'Johnny Depp'),
(103, 'Mark Ruffalo'),
(109, 'Elijah Wood'),
(110, 'Viggo Mortensen'),
(114, 'Orlando Bloom'),
(116, 'Keira Knightley'),
(118, 'Geoffrey Rush'),
(140, 'Lucy Liu'),
(194, 'Richard Harris'),
(198, 'Dan Castellaneta'),
(199, 'Julie Kavner'),
(200, 'Nancy Cartwright'),
(204, 'Kate Winslet'),
(206, 'Jim Carrey'),
(228, 'Ed Harris'),
(335, 'Michael Shannon'),
(454, 'Michael PeÃ±a'),
(515, 'Glenn Close'),
(517, 'Pierce Brosnan'),
(524, 'Natalie Portman'),
(536, 'Jorja Fox'),
(880, 'Ben Affleck'),
(882, 'Liv Tyler'),
(884, 'Steve Buscemi'),
(935, 'Connie Nielsen'),
(976, 'Jason Statham'),
(1245, 'Scarlett Johansson'),
(1269, 'Kevin Costner'),
(1283, 'Helena Bonham Carter'),
(1327, 'Ian McKellen'),
(1328, 'Sean Astin'),
(1333, 'Andy Serkis'),
(1640, 'Stellan SkarsgÃ¥rd'),
(1709, 'Jack Davenport'),
(1772, 'Anna Faris'),
(1813, 'Anne Hathaway'),
(1893, 'Casey Affleck'),
(1894, 'Scott Caan'),
(1909, 'Conchata Ferrell'),
(1951, 'Elisabeth Shue'),
(2038, 'Naomie Harris'),
(2387, 'Patrick Stewart'),
(2778, 'Dennis Hopper'),
(2954, 'Jeffrey Wright'),
(2978, 'Max Minghella'),
(3223, 'Robert Downey Jr.'),
(3266, 'Joe Mantegna'),
(3391, 'Kathleen Turner'),
(3895, 'Michael Caine'),
(3905, 'William H. Macy'),
(4173, 'Anthony Hopkins'),
(4491, 'Jennifer Aniston'),
(4566, 'Alan Rickman'),
(4730, 'Emmy Rossum'),
(4756, 'Matthew Broderick'),
(4808, 'Giancarlo Esposito'),
(4886, 'Norman Reedus'),
(5064, 'Meryl Streep'),
(5081, 'Emily Blunt'),
(5170, 'Mike Starr'),
(5292, 'Denzel Washington'),
(5293, 'Willem Dafoe'),
(5313, 'Lena Olin'),
(5374, 'Jim Parsons'),
(5470, 'Kristin Scott Thomas'),
(5530, 'James McAvoy'),
(5577, 'Chris O\'Donnell'),
(5586, 'Yeardley Smith'),
(5644, 'Alexandra Maria Lara'),
(5661, 'Matthew Gray Gubler'),
(5723, 'John Leguizamo'),
(6065, 'Dennis Quaid'),
(6240, 'Mariska Hargitay'),
(6279, 'Alexis Bledel'),
(6413, 'Danny Huston'),
(6885, 'Charlize Theron'),
(6944, 'Octavia Spencer'),
(6952, 'Charlie Sheen'),
(6968, 'Hugh Jackman'),
(7060, 'Martin Freeman'),
(7062, 'Andrew Lincoln'),
(7132, 'Vincent D\'Onofrio'),
(7497, 'Holt McCallany'),
(7639, 'Joan Bennett'),
(8169, 'Tyrese Gibson'),
(8395, 'Luke Perry'),
(8437, 'Teri Garr'),
(8447, 'Jeff Daniels'),
(8536, 'Victor Garber'),
(8691, 'Zoe Saldana'),
(8984, 'Bill Pullman'),
(9012, 'Jonny Lee Miller'),
(9030, 'Thandie Newton'),
(9048, 'Clark Gregg'),
(9137, 'RenÃ©e Zellweger'),
(9206, 'Neve Campbell'),
(9273, 'Amy Adams'),
(9278, 'Jennifer Garner'),
(9280, 'Ellen Pompeo'),
(9464, 'Harry Lennix'),
(9599, 'Cloris Leachman'),
(9657, 'Patrick Warburton'),
(9705, 'Takahiro Sakurai'),
(9720, 'Hideo Ishikawa'),
(10297, 'Matthew McConaughey'),
(10696, 'Famke Janssen'),
(10698, 'TchÃ©ky Karyo'),
(10727, 'John Hannah'),
(10825, 'Donal Logue'),
(10980, 'Daniel Radcliffe'),
(10989, 'Rupert Grint'),
(10990, 'Emma Watson'),
(10993, 'Tom Felton'),
(11006, 'James Marsden'),
(11263, 'Peter FranzÃ©n'),
(11318, 'Holland Taylor'),
(11680, 'Pauley Perrette'),
(11701, 'Angelina Jolie'),
(11824, 'Tom Welling'),
(11864, 'Ryan Phillippe'),
(12077, 'Jim Cummings'),
(12122, 'Ron Rifkin'),
(12516, 'Linda Hunt'),
(12763, 'Joseph Fiennes'),
(12795, 'Nikolaj Coster-Waldau'),
(12835, 'Vin Diesel'),
(12836, 'Ted Danson'),
(12982, 'Peter Capaldi'),
(13548, 'James Spader'),
(13922, 'Seth Green'),
(14329, 'Dean Norris'),
(14405, 'Courteney Cox'),
(14406, 'Lisa Kudrow'),
(14407, 'Matt LeBlanc'),
(14408, 'Matthew Perry'),
(14409, 'David Schwimmer'),
(15232, 'Ty Burrell'),
(15423, 'Paget Brewster'),
(15424, 'Michael Vartan'),
(16377, 'Jerry Seinfeld'),
(16478, 'Johnny Galecki'),
(16752, 'David Harewood'),
(16828, 'Chris Evans'),
(16856, 'Gabriel Macht'),
(16940, 'Jeremy Irons'),
(17005, 'Doug Jones'),
(17236, 'A.J. Cook'),
(17245, 'Ben McKenzie'),
(17276, 'Gerard Butler'),
(17286, 'Lena Headey'),
(17287, 'Dominic West'),
(17402, 'Ron Livingston'),
(17419, 'Bryan Cranston'),
(17605, 'Idris Elba'),
(17647, 'Michelle Rodriguez'),
(17697, 'John Krasinski'),
(18050, 'Elle Fanning'),
(18054, 'Rinko Kikuchi'),
(18324, 'Steve Zahn'),
(18514, 'Asia Argento'),
(18616, 'David Morrissey'),
(18918, 'Dwayne Johnson'),
(18973, 'Mila Kunis'),
(18974, 'Laura Prepon'),
(18977, 'Ed O\'Neill'),
(18992, 'Aidan Quinn'),
(18997, 'Bryce Dallas Howard'),
(19034, 'Evangeline Lilly'),
(19154, 'Daniel Gillies'),
(19211, 'Ian Somerhalder'),
(19292, 'Adam Sandler'),
(19392, 'Jesse L. Martin'),
(19536, 'Josh Duhamel'),
(19588, 'Miyu Irino'),
(19728, 'Mark Harmon'),
(19976, 'Robert Joy'),
(20373, 'Arielle Kebbel'),
(20746, 'Justin Chambers'),
(21041, 'Shohreh Aghdashloo'),
(21127, 'Bobby Cannavale'),
(21356, 'D.B. Woodside'),
(21411, 'Ice-T'),
(21625, 'Molly Ringwald'),
(21702, 'Ming-Na Wen'),
(22226, 'Paul Rudd'),
(22809, 'Keeley Hawes'),
(22810, 'Andy Nyman'),
(22970, 'Peter Dinklage'),
(23498, 'Alexander Ludwig'),
(23680, 'Dee Bradley Baker'),
(23987, 'Jouji Nakata'),
(24343, 'Peter Mayhew'),
(24357, 'Alex Borstein'),
(24813, 'David McCallum'),
(25258, 'Rossy de Palma'),
(25663, 'Thomas Brodie-Sangster'),
(25934, 'David Boreanaz'),
(26209, 'Matt Lucas'),
(26723, 'Katheryn Winnick'),
(27740, 'Walton Goggins'),
(28633, 'Richard Jenkins'),
(28848, 'Sean Pertwee'),
(29020, 'Eric Christian Olsen'),
(29382, 'Carl Lumbly'),
(30084, 'Anna Torv'),
(30614, 'Ryan Gosling'),
(31167, 'Elizabeth Mitchell'),
(31171, 'Julie Bowen'),
(31535, 'Melissa McBride'),
(31838, 'Danielle Panabaker'),
(32798, 'Elisabeth Moss'),
(32895, 'Kevin James'),
(32987, 'Sam Riley'),
(33192, 'Joel Edgerton'),
(33337, 'Eric Johnson'),
(34065, 'Santiago Cabrera'),
(34485, 'Lauren Holly'),
(34715, 'James Nesbitt'),
(34900, 'Paul Whitehouse'),
(35013, 'David Oyelowo'),
(35091, 'Scott Grimes'),
(35317, 'Kate Mulgrew'),
(35759, 'Satish Shah'),
(36424, 'LL Cool J'),
(36594, 'Juno Temple'),
(37014, 'Lauren German'),
(37089, 'Cameron Monaghan'),
(37260, 'Abbie Cornish'),
(37600, 'Wendy Schaal'),
(38940, 'Evan Rachel Wood'),
(38941, 'Jim Sturgess'),
(39189, 'Stephen Merchant'),
(39464, 'Tamara Taylor'),
(39658, 'Sally Hawkins'),
(39995, 'Michael Cera'),
(40327, 'TÅru Furuya'),
(41297, 'Alex O\'Loughlin'),
(42747, 'Misha Collins'),
(44079, 'Charlotte Rampling'),
(44735, 'Jesse Eisenberg'),
(45407, 'Mark Duplass'),
(47296, 'Jeffrey Dean Morgan'),
(47647, 'Christian Rubeck'),
(49623, 'Jared Padalecki'),
(49624, 'Jensen Ackles'),
(49706, 'Adam RodrÃ­guez'),
(50463, 'Malin Ã…kerman'),
(51329, 'Bradley Cooper'),
(51682, 'Wes Chatham'),
(51684, 'Mehcad Brooks'),
(52139, 'Seth MacFarlane'),
(52462, 'Betty Buckley'),
(52605, 'Eva Longoria'),
(53280, 'Tyler Posey'),
(53862, 'Kaley Cuoco'),
(53863, 'Simon Helberg'),
(54815, 'Steven Strait'),
(55470, 'Dominic Cooper'),
(55775, 'Katie Cassidy'),
(56614, 'Ray Stevenson'),
(56679, 'Edwin Hodge'),
(56730, 'Cole Sprouse'),
(56930, 'David Selby'),
(57755, 'Woody Harrelson'),
(58395, 'Karin Konoval'),
(59214, 'Cas Anvar'),
(59216, 'Tom Cavanagh'),
(59254, 'Pablo Schreiber'),
(59262, 'Robin Lord Taylor'),
(60950, 'David Spade'),
(62064, 'Chris Pine'),
(62168, 'Sasha Pieterse'),
(62220, 'Lauren Cohan'),
(62553, 'Samantha Smith'),
(62561, 'Tessa Thompson'),
(62861, 'Andy Samberg'),
(63312, 'Yvonne Strahovski'),
(63522, 'SofÃ­a Vergara'),
(64436, 'Daniel Wu'),
(65198, 'Kane Kosugi'),
(65510, 'Mayumi Tanaka'),
(65760, 'Marin Hinkle'),
(65871, 'Amanda Schull'),
(66791, 'Anastasia Hille'),
(68470, 'Minami Takayama'),
(68472, 'Kappei Yamaguchi'),
(69210, 'Chyler Leigh'),
(69718, 'Jon Cryer'),
(69921, 'Paul Shaffer'),
(71070, 'Amanda Seyfried'),
(72129, 'Jennifer Lawrence'),
(73044, 'Ikue Otani'),
(73381, 'Matthias Schoenaerts'),
(73457, 'Chris Pratt'),
(73968, 'Henry Cavill'),
(74541, 'Corey Stoll'),
(74568, 'Chris Hemsworth'),
(76070, 'Mia Wasikowska'),
(76095, 'Jennifer Finnigan'),
(76110, 'Stephen Amell'),
(76793, 'Irrfan Khan'),
(77117, 'Angus T. Jones'),
(77547, 'Brady Bluhm'),
(77700, 'Travis Fimmel'),
(77933, 'Yuriko Yamaguchi'),
(77948, 'Selena Gomez'),
(78030, 'Ashley Benson'),
(78062, 'Aml Ameen'),
(78110, 'Scott Adkins'),
(79505, 'Joseph Morgan'),
(81244, 'Akira Ishida'),
(81668, 'Rachael MacFarlane'),
(81685, 'Frank Grillo'),
(82055, 'Junko Takeuchi'),
(82057, 'Chie Nakamura'),
(82104, 'Danai Gurira'),
(82191, 'Sharlto Copley'),
(83002, 'Jessica Chastain'),
(83141, 'Paul Wesley'),
(84224, 'Christian Serratos'),
(84497, 'Aaron Paul'),
(84505, 'Hiroaki Hirata'),
(84507, 'Kazuya Nakai'),
(85144, 'Candice King'),
(86017, 'Shakti Kapoor'),
(86468, 'Sarah Wayne Callies'),
(87582, 'Wakana Yamazaki'),
(87722, 'Noomi Rapace'),
(89968, 'Kazuhiko Inoue'),
(90133, 'Akemi Okamura'),
(90633, 'Gal Gadot'),
(91606, 'Tom Hiddleston'),
(93803, 'Mamoru Miyano'),
(94185, 'Shailene Woodley'),
(94423, 'Shantel VanSanten'),
(101336, 'Kathryn Leigh Scott'),
(104632, 'Sylvia Hoeks'),
(105061, 'Jonathan Frid'),
(110076, 'Nicholas Burns'),
(111467, 'April Stewart'),
(114000, 'Kevin Rankin'),
(114460, 'Alexandra JimÃ©nez'),
(115150, 'Kaya Scodelario'),
(115858, 'Cotter Smith'),
(116474, 'Kevin Alejandro'),
(116774, 'David Ramsey'),
(117484, 'George Eads'),
(117627, 'Lara Parker'),
(117642, 'Jason Momoa'),
(118545, 'Dakota Johnson'),
(120679, 'Eddie McClintock'),
(122237, 'Michaela Conlin'),
(122616, 'Grant Gustin'),
(122756, 'DulÃ© Hill'),
(123727, 'Kirsten Vangsness'),
(123813, 'Ian Anthony Dale'),
(125025, 'Joey King'),
(127733, 'Bob Morley'),
(129104, 'Melissa Benoist'),
(132157, 'Ezra Miller'),
(132213, 'Ethan Cutkosky'),
(134531, 'Anna Gunn'),
(135651, 'Michael B. Jordan'),
(136487, 'David Letterman'),
(140114, 'Marie Avgeropoulos'),
(141876, 'Roland MÃ¸ller'),
(142193, 'Diego Klattenhoff'),
(142389, 'Charlie Rowe'),
(144655, 'Tomokazu Sugita'),
(144852, 'Richard Harmon'),
(145247, 'Phoebe Tonkin'),
(149894, 'Yuuki Kaji'),
(151263, 'T. J. Thyne'),
(154856, 'Sarah Rafferty'),
(156962, 'Eric Stonestreet'),
(159962, 'Kat Graham'),
(164431, 'Amir Arison'),
(172069, 'Chadwick Boseman'),
(173849, 'Eric Szmanda'),
(176497, 'Rachel Melvin'),
(177231, 'Lana Parrilla'),
(182272, 'Cynthia Addai-Robinson'),
(192944, 'Tom Ellis'),
(204815, 'Jesse Tyler Ferguson'),
(205307, 'Lucy Hale'),
(206905, 'Jeremy Allen White'),
(208099, 'Kunal Nayyar'),
(208782, 'Holland Roden'),
(209674, 'RJ Mitte'),
(211993, 'Caitlin Fitzgerald'),
(212700, 'Candice Patton'),
(221019, 'Daniela Ruah'),
(221611, 'Jonathan Groff'),
(221809, 'Taylor Schilling'),
(221841, 'Zach Roerig'),
(222088, 'Shay Mitchell'),
(222141, 'Megan Boone'),
(224513, 'Ana de Armas'),
(224870, 'Paco LeÃ³n'),
(227454, 'Alicia Vikander'),
(227615, 'Mika Hijii'),
(235767, 'Colin O\'Donoghue'),
(236465, 'Grayson Hall'),
(236695, 'John Boyega'),
(236696, 'Terry Notary'),
(237076, 'Rikiya Koyama'),
(237569, 'Rafael Amaya'),
(239019, 'Kit Harington'),
(239271, 'Jeremy Strong'),
(239574, 'Eugenio Derbez'),
(277081, 'Troian Bellisario'),
(303020, 'Meaghan Rath'),
(467645, 'Boyd Holbrook'),
(496470, 'Jake Lacy'),
(512316, 'Jacob Latimore'),
(515875, 'Lesley-Ann Brandt'),
(518627, 'Joel Courtney'),
(527393, 'Dylan O\'Brien'),
(543530, 'Dave Bautista'),
(559643, 'Seychelle Gabriel'),
(587020, 'Theo James'),
(833798, 'Tim Man'),
(928572, 'Scott Eastwood'),
(934173, 'David Mazouz'),
(935235, 'Marwan Kenzari'),
(947514, 'Itziar ItuÃ±o'),
(956764, 'Iain De Caestecker'),
(973667, 'Rosa Salazar'),
(991980, 'Tyler Blackburn'),
(996701, 'Miles Teller'),
(1001657, 'Sophie Turner'),
(1003086, 'Josefin Asplund'),
(1010157, 'Diane Guerrero'),
(1016168, 'Lily James'),
(1023139, 'Adam Driver'),
(1027740, 'Paco Tous'),
(1034197, 'Samira Wiley'),
(1042728, 'Ãšrsula CorberÃ³'),
(1049916, 'Hannah Gross'),
(1075037, 'Danielle Brooks'),
(1077775, 'Alexander Calvert'),
(1102621, 'Vithaya Pansringarm'),
(1108907, 'Nick Robinson'),
(1109836, 'Pedro Alonso'),
(1110405, 'Mackenzie Davis'),
(1136940, 'Lili Reinhart'),
(1144694, 'Christopher Larkin'),
(1148626, 'Lindsey Morgan'),
(1151657, 'Jing Tian'),
(1152019, 'Ashton Sanders'),
(1181303, 'Chloe Bennet'),
(1181353, 'Ryan Eggold'),
(1188558, 'Jeremy Jordan'),
(1202689, 'Jessica Henwick'),
(1211093, 'Brant Daugherty'),
(1213278, 'Eliza Taylor'),
(1213580, 'Brian Dietzen'),
(1215525, 'Jorge Garcia'),
(1216133, 'Rick Hoffman'),
(1217657, 'Robert David Hall'),
(1217934, 'Betsy Brandt'),
(1221085, 'Jessica Sula'),
(1223171, 'Willa Holland'),
(1223786, 'Emilia Clarke'),
(1223787, 'Finn Jones'),
(1223886, 'Elizabeth Henstridge'),
(1224027, 'Emily Bett Rickards'),
(1224051, 'Jon Michael Hill'),
(1230381, 'Sean Murray'),
(1252801, 'Emma Kenney'),
(1253360, 'Pedro Pascal'),
(1254583, 'Jamie Dornan'),
(1255907, 'Echo Kellum'),
(1257816, 'Charles Michael Davis'),
(1267329, 'Lupita Nyong\'o'),
(1270551, 'Dada Kondke'),
(1283843, 'Alba Flores'),
(1284159, 'Simon Baker'),
(1286328, 'Haley Lu Richardson'),
(1310760, 'Ki Hong Lee'),
(1315036, 'Daisy Ridley'),
(1327278, 'Antara Biswas'),
(1332813, 'Swapna'),
(1340020, 'Ãlvaro Morte'),
(1344344, 'Storm Reid'),
(1347065, 'Abigail Hardingham'),
(1360421, 'Brody Rose'),
(1361876, 'Dominique Tipper'),
(1379277, 'Alex Lawther'),
(1389339, 'Blake Cooper'),
(1394429, 'Jordan Patrick Smith'),
(1395348, 'Sasheer Zamata'),
(1397346, 'Beulah Koale'),
(1397778, 'Anya Taylor-Joy'),
(1400939, 'Jacqueline Byers'),
(1428398, 'Carlos Valdes'),
(1443625, 'Jackie Cruz'),
(1457004, 'Oscar MartÃ­nez'),
(1464650, 'Dafne Keen'),
(1465190, 'Michael Malarkey'),
(1475647, 'Chris Wood'),
(1492327, 'Cameron Esposito'),
(1516211, 'Emily Maddison'),
(1590759, 'Noah Jupe'),
(1590768, 'FÃ¡bio Lucindo'),
(1592855, 'Madelaine Petsch'),
(1609347, 'Alex HÃ¸gh Andersen'),
(1609794, 'Pearl Mackie'),
(1680339, 'K.J. Apa'),
(1683343, 'Cailee Spaeny'),
(1718318, 'Sonny Valicenti'),
(1721740, 'Camila Mendes'),
(1815539, 'Millicent Simmonds'),
(1835721, 'Iwami Manaka'),
(1869887, 'J. Michael Finley'),
(1924769, 'Cade Woodward'),
(2034418, 'Jacob Elordi'),
(2034419, 'Ghayas'),
(2034420, 'Jessie Usher'),
(2034421, 'Samuel L. Jackson'),
(2034422, 'Richard Roundtree'),
(2034423, 'Alexandra Shipp'),
(2034424, 'Regina Hall'),
(2034425, 'Tom Holland'),
(2034426, 'Jake Gyllenhaal'),
(2034427, 'Marisa Tomei'),
(2034428, 'Jon Favreau');

-- --------------------------------------------------------

--
-- Table structure for table `actor_movies`
--

CREATE TABLE `actor_movies` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `actor_movies`
--

INSERT INTO `actor_movies` (`id`, `movie_id`) VALUES
(2, 11),
(3, 11),
(4, 11),
(24343, 11),
(6, 11),
(85, 22),
(118, 22),
(114, 22),
(116, 22),
(1709, 22),
(109, 120),
(1327, 120),
(110, 120),
(1328, 120),
(882, 120),
(10980, 671),
(10989, 671),
(10990, 671),
(194, 671),
(10993, 671),
(16377, 5559),
(9137, 5559),
(12077, 5559),
(4756, 5559),
(9657, 5559),
(206, 8467),
(8447, 8467),
(34485, 8467),
(8437, 8467),
(5170, 8467),
(74568, 10195),
(524, 10195),
(91606, 10195),
(4173, 10195),
(1640, 10195),
(1284159, 11683),
(5723, 11683),
(2778, 11683),
(18514, 11683),
(19976, 11683),
(3223, 24428),
(16828, 24428),
(103, 24428),
(74568, 24428),
(1245, 24428),
(206, 100042),
(8447, 100042),
(176497, 100042),
(3391, 100042),
(77547, 100042),
(11701, 102651),
(18050, 102651),
(82191, 102651),
(32987, 102651),
(36594, 102651),
(22226, 102899),
(19034, 102899),
(74541, 102899),
(21127, 102899),
(454, 102899),
(73457, 118340),
(8691, 118340),
(543530, 118340),
(12835, 118340),
(51329, 118340),
(73457, 135397),
(18997, 135397),
(76793, 135397),
(7132, 135397),
(1108907, 135397),
(880, 141052),
(73968, 141052),
(90633, 141052),
(132157, 141052),
(117642, 141052),
(10297, 157336),
(83002, 157336),
(1813, 157336),
(3895, 157336),
(1893, 157336),
(94185, 157350),
(587020, 157350),
(204, 157350),
(996701, 157350),
(56614, 157350),
(19292, 159824),
(62861, 159824),
(77948, 159824),
(32895, 159824),
(884, 159824),
(78110, 180894),
(1102621, 180894),
(65198, 180894),
(833798, 180894),
(227615, 180894),
(2, 181808),
(4, 181808),
(1023139, 181808),
(1315036, 181808),
(236695, 181808),
(527393, 198663),
(115150, 198663),
(1310760, 198663),
(78062, 198663),
(1389339, 198663),
(880, 209112),
(73968, 209112),
(90633, 209112),
(9273, 209112),
(44735, 209112),
(76070, 241259),
(85, 241259),
(1813, 241259),
(1283, 241259),
(4566, 241259),
(6968, 263115),
(2387, 263115),
(1464650, 263115),
(467645, 263115),
(39189, 263115),
(236695, 268896),
(928572, 268896),
(1683343, 268896),
(1151657, 268896),
(18054, 268896),
(17276, 274855),
(5644, 274855),
(38941, 274855),
(37260, 274855),
(228, 274855),
(1333, 281338),
(57755, 281338),
(18324, 281338),
(58395, 281338),
(236696, 281338),
(73457, 283995),
(8691, 283995),
(543530, 283995),
(12835, 283995),
(51329, 283995),
(172069, 284054),
(135651, 284054),
(1267329, 284054),
(82104, 284054),
(7060, 284054),
(90633, 297762),
(62064, 297762),
(935, 297762),
(32, 297762),
(6413, 297762),
(31167, 316727),
(81685, 316727),
(34, 316727),
(56679, 316727),
(30614, 335984),
(224513, 335984),
(32, 335984),
(104632, 335984),
(3, 335984),
(527393, 336843),
(115150, 336843),
(25663, 336843),
(1310760, 336843),
(973667, 336843),
(12835, 337339),
(976, 337339),
(18918, 337339),
(17647, 337339),
(8169, 337339),
(227454, 338970),
(27740, 338970),
(17287, 338970),
(64436, 338970),
(5470, 338970),
(35013, 340022),
(6885, 340022),
(33192, 340022),
(71070, 340022),
(9030, 340022),
(5292, 345887),
(1253360, 345887),
(1152019, 345887),
(8984, 345887),
(512316, 347882),
(559643, 347882),
(1395348, 347882),
(1344344, 347882),
(1492327, 347882),
(19536, 375098),
(10696, 375098),
(4808, 375098),
(86468, 375098),
(211993, 375098),
(5530, 381288),
(1397778, 381288),
(52462, 381288),
(1286328, 381288),
(1221085, 381288),
(83002, 396371),
(17605, 396371),
(1269, 396371),
(39995, 396371),
(239271, 396371),
(39658, 399055),
(17005, 399055),
(335, 399055),
(28633, 399055),
(6944, 399055),
(19292, 400155),
(62861, 400155),
(77948, 400155),
(32895, 400155),
(60950, 400155),
(6885, 400579),
(1110405, 400579),
(45407, 400579),
(17402, 400579),
(72129, 401981),
(33192, 401981),
(73381, 401981),
(44079, 401981),
(16940, 401981),
(87722, 406990),
(935235, 406990),
(47647, 406990),
(5293, 406990),
(515, 406990),
(18918, 427641),
(2038, 427641),
(50463, 427641),
(47296, 427641),
(496470, 427641),
(22810, 429417),
(7060, 429417),
(1379277, 429417),
(34900, 429417),
(110076, 429417),
(114460, 437033),
(224870, 437033),
(25258, 437033),
(1457004, 437033),
(18918, 447200),
(9206, 447200),
(59254, 447200),
(141876, 447200),
(114000, 447200),
(5081, 447332),
(17697, 447332),
(1815539, 447332),
(1590759, 447332),
(1924769, 447332),
(1772, 454619),
(239574, 454619),
(52605, 454619),
(1516211, 454619),
(10727, 454619),
(125025, 454983),
(518627, 454983),
(2034418, 454983),
(21625, 454983),
(71070, 458423),
(1016168, 458423),
(55470, 458423),
(5064, 458423),
(517, 458423),
(6065, 470878),
(9599, 470878),
(1869887, 470878),
(1360421, 470878),
(1835721, 476292),
(19588, 476292),
(144655, 476292),
(93803, 492719),
(149894, 492719),
(144655, 492719),
(9705, 492719),
(68470, 493006),
(87582, 493006),
(237076, 493006),
(40327, 493006),
(111467, 2190),
(73044, 60572),
(1590768, 60572),
(200, 456),
(198, 456),
(199, 456),
(5586, 456),
(17419, 1396),
(134531, 1396),
(84497, 1396),
(14329, 1396),
(1217934, 1396),
(209674, 1396),
(1223786, 1399),
(239019, 1399),
(22970, 1399),
(17286, 1399),
(12795, 1399),
(1001657, 1399),
(7062, 1402),
(4886, 1402),
(62220, 1402),
(82104, 1402),
(84224, 1402),
(31535, 1402),
(9048, 1403),
(1181303, 1403),
(956764, 1403),
(21702, 1403),
(1223886, 1403),
(76110, 1412),
(55775, 1412),
(1224027, 1412),
(116774, 1412),
(1255907, 1412),
(1223171, 1412),
(9012, 1415),
(140, 1415),
(1224051, 1415),
(18992, 1415),
(9280, 1416),
(20746, 1416),
(5374, 1418),
(16478, 1418),
(53862, 1418),
(53863, 1418),
(208099, 1418),
(31171, 1421),
(15232, 1421),
(204815, 1421),
(156962, 1421),
(63522, 1421),
(18977, 1421),
(221809, 1424),
(18974, 1424),
(1075037, 1424),
(1010157, 1424),
(1443625, 1424),
(35317, 1424),
(12836, 1431),
(1951, 1431),
(117484, 1431),
(536, 1431),
(173849, 1431),
(1217657, 1431),
(52139, 1433),
(37600, 1433),
(81668, 1433),
(35091, 1433),
(23680, 1433),
(52139, 1434),
(24357, 1434),
(18973, 1434),
(13922, 1434),
(49624, 1622),
(49623, 1622),
(42747, 1622),
(1077775, 1622),
(62553, 1622),
(14409, 1668),
(14405, 1668),
(14408, 1668),
(4491, 1668),
(14407, 1668),
(14406, 1668),
(25934, 1911),
(122237, 1911),
(151263, 1911),
(39464, 1911),
(9278, 2046),
(15424, 2046),
(12122, 2046),
(29382, 2046),
(8536, 2046),
(5313, 2046),
(6952, 2691),
(69718, 2691),
(77117, 2691),
(65760, 2691),
(11318, 2691),
(1909, 2691),
(6240, 2734),
(21411, 2734),
(3266, 4057),
(5661, 4057),
(15423, 4057),
(17236, 4057),
(123727, 4057),
(49706, 4057),
(19728, 4614),
(11680, 4614),
(1230381, 4614),
(24813, 4614),
(1213580, 4614),
(5577, 17610),
(36424, 17610),
(221019, 17610),
(29020, 17610),
(12516, 17610),
(83141, 18165),
(19211, 18165),
(159962, 18165),
(85144, 18165),
(1465190, 18165),
(221841, 18165),
(277081, 31917),
(78030, 31917),
(205307, 31917),
(222088, 31917),
(62168, 31917),
(991980, 31917),
(41297, 32798),
(1894, 32798),
(123813, 32798),
(303020, 32798),
(1215525, 32798),
(1397346, 32798),
(3905, 34307),
(4730, 34307),
(206905, 34307),
(37089, 34307),
(1252801, 34307),
(132213, 34307),
(53280, 34524),
(527393, 34524),
(208782, 34524),
(16856, 37680),
(1216133, 37680),
(154856, 37680),
(65871, 37680),
(122756, 37680),
(177231, 39272),
(235767, 39272),
(77700, 44217),
(1609347, 44217),
(26723, 44217),
(23498, 44217),
(1394429, 44217),
(1003086, 44217),
(11263, 44217),
(237569, 44953),
(79505, 46896),
(19154, 46896),
(145247, 46896),
(1257816, 46896),
(13548, 46952),
(222141, 46952),
(142193, 46952),
(1181353, 46952),
(164431, 46952),
(9464, 46952),
(1213278, 48866),
(127733, 48866),
(140114, 48866),
(1148626, 48866),
(144852, 48866),
(1144694, 48866),
(12982, 57243),
(1609794, 57243),
(26209, 57243),
(17245, 60708),
(10825, 60708),
(934173, 60708),
(28848, 60708),
(59262, 60708),
(122616, 60735),
(212700, 60735),
(19392, 60735),
(59216, 60735),
(31838, 60735),
(1428398, 60735),
(18616, 61555),
(34715, 61555),
(22809, 61555),
(1347065, 61555),
(10698, 61555),
(66791, 61555),
(1223787, 62127),
(1202689, 62127),
(129104, 62688),
(69210, 62688),
(16752, 62688),
(1475647, 62688),
(1188558, 62688),
(51684, 62688),
(192944, 63174),
(37014, 63174),
(116474, 63174),
(21356, 63174),
(515875, 63174),
(11824, 63174),
(38940, 63247),
(9030, 63247),
(2954, 63247),
(11006, 63247),
(62561, 63247),
(228, 63247),
(54815, 63639),
(59214, 63639),
(1361876, 63639),
(51682, 63639),
(21041, 63639),
(182272, 66857),
(120679, 66857),
(94423, 66857),
(11864, 66857),
(221611, 67744),
(7497, 67744),
(1049916, 67744),
(1718318, 67744),
(30084, 67744),
(115858, 67744),
(1680339, 69050),
(1721740, 69050),
(1136940, 69050),
(56730, 69050),
(1592855, 69050),
(8395, 69050),
(32798, 69478),
(63312, 69478),
(12763, 69478),
(2978, 69478),
(1034197, 69478),
(6279, 69478),
(34065, 70513),
(76095, 70513),
(142389, 70513),
(1400939, 70513),
(1042728, 71446),
(947514, 71446),
(1340020, 71446),
(1027740, 71446),
(1109836, 71446),
(1283843, 71446),
(2034420, 493007),
(2034421, 493007),
(2034422, 493007),
(2034423, 493007),
(2034424, 493007),
(2034425, 493008),
(2034426, 493008),
(2034421, 493008),
(2034427, 493008),
(2034428, 493008);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `movie_id`, `created_at`, `updated_at`, `text`) VALUES
(3, 19, 345887, '2019-02-26 06:19:59', '2019-02-26 06:19:59', 'Broooo'),
(4, 19, 345887, '2019-02-26 06:34:15', '2019-02-26 06:34:15', 'Yayayay'),
(5, 19, 345887, '2019-02-26 06:35:39', '2019-02-26 06:35:39', 'Its me again!!'),
(6, 17, 345887, '2019-02-26 06:42:11', '2019-02-26 06:42:11', 'Isisis'),
(7, 17, 458423, '2019-02-26 07:21:13', '2019-02-26 07:21:13', 'Wow'),
(8, 22, 456, '2019-02-27 07:48:36', '2019-02-27 07:48:36', 'Oi!'),
(9, 22, 456, '2019-02-27 07:48:58', '2019-02-27 07:48:58', 'Lorem ipsum omingsnsjsisusudhsjwjsixjdjwjxjskckwodoqowposcivudjejcososocjwkfiwocienchc uekentbsici289r'),
(10, 22, 22, '2019-02-27 08:40:53', '2019-02-27 08:40:53', 'Best!'),
(11, 19, 1418, '2019-02-28 08:04:39', '2019-02-28 08:04:39', 'Hey!!'),
(12, 19, 345887, '2019-03-08 06:49:43', '2019-03-08 06:49:43', 'Hello!!'),
(13, 19, 345887, '2019-03-08 07:49:20', '2019-03-08 07:49:20', 'Ai!'),
(14, 25, 345887, '2019-06-26 11:12:45', '2019-06-26 11:12:45', 'Checking Harry'),
(15, 25, 345887, '2019-06-26 11:16:31', '2019-06-26 11:16:31', 'Checking THings'),
(16, 25, 345887, '2019-06-26 11:17:00', '2019-06-26 11:17:00', 'Hey Hey'),
(17, 25, 345887, '2019-06-26 11:31:04', '2019-06-26 11:31:04', 'lll'),
(18, 25, 345887, '2019-06-26 11:32:25', '2019-06-26 11:32:25', 'Checking'),
(19, 25, 345887, '2019-06-26 11:33:38', '2019-06-26 11:33:38', 'LOL'),
(20, 25, 345887, '2019-06-26 11:35:02', '2019-06-26 11:35:02', 'YOYO!!'),
(21, 16, 345887, '2019-06-27 09:02:34', '2019-06-27 09:02:35', 'Ok Done.'),
(22, 16, 427641, '2019-06-27 09:03:03', '2019-06-27 09:03:03', 'Check'),
(23, 16, 335984, '2019-06-27 12:01:49', '2019-06-27 12:01:49', 'Yo!'),
(24, 16, 345887, '2019-06-27 14:51:21', '2019-06-27 14:51:21', 'As well.'),
(25, 16, 345887, '2019-06-27 23:56:33', '2019-06-27 23:56:33', 'Add more!'),
(26, 16, 447200, '2019-06-28 09:04:54', '2019-06-28 09:04:54', 'HHOOOOEEKKKLL'),
(27, 16, 447200, '2019-06-28 09:05:00', '2019-06-28 09:05:00', 'HHOOOOEEKKKLLaaa'),
(28, 16, 102899, '2019-06-28 09:05:48', '2019-06-28 09:05:48', 'HEHE'),
(29, 16, 345887, '2019-06-30 08:55:33', '2019-06-30 08:55:33', 'kjbkjb'),
(30, 16, 1412, '2019-07-03 00:23:27', '2019-07-03 00:23:27', 'KJBSFKJ'),
(31, 16, 493007, '2019-07-05 11:40:10', '2019-07-05 11:40:10', 'YEAH!'),
(32, 16, 274855, '2019-07-06 15:04:00', '2019-07-06 15:04:00', 'comm');

-- --------------------------------------------------------

--
-- Table structure for table `episodes`
--

CREATE TABLE `episodes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `length` time NOT NULL,
  `seriesID` int(10) UNSIGNED NOT NULL,
  `seasonNumber` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `movie` int(10) UNSIGNED NOT NULL,
  `user` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`movie`, `user`) VALUES
(11683, 19),
(11, 19),
(159824, 19),
(447200, 19),
(345887, 17),
(458423, 17),
(456, 22),
(447200, 22),
(22, 22),
(32798, 22),
(345887, 23),
(11, 23),
(1412, 19),
(671, 19),
(345887, 19),
(63639, 19),
(71446, 19),
(492719, 19),
(1433, 19),
(476292, 19),
(429417, 19),
(60708, 24),
(120, 24);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `movie_genres_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`movie_genres_id`, `name`) VALUES
(12, 'Adventure'),
(14, 'Fantasy'),
(16, 'Animation'),
(18, 'Drama'),
(27, 'Horror'),
(28, 'Action'),
(35, 'Comedy'),
(36, 'History'),
(37, 'Western'),
(53, 'Thriller'),
(80, 'Crime'),
(99, 'Documentary'),
(878, 'Science Fiction'),
(9648, 'Mystery'),
(10402, 'Music'),
(10749, 'Romance'),
(10751, 'Family'),
(10752, 'War'),
(10759, 'Action & Adventure'),
(10762, 'Kids'),
(10763, 'News'),
(10764, 'Reality'),
(10765, 'Sci-Fi & Fantasy'),
(10766, 'Soap'),
(10767, 'Talk'),
(10768, 'War & Politics'),
(10770, 'TV Movie'),
(10771, 'Check'),
(10772, 'Check2');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_08_133807_create_actors_table', 1),
(4, '2018_11_08_135222_create_actor_movies_table', 2),
(5, '2018_11_08_135854_create_movies_table', 3),
(6, '2018_11_08_141210_add_foriegn_keys_to_actor_movies', 4),
(7, '2018_11_08_143137_create_episodes_table', 5),
(8, '2018_11_08_143532_create_genres_table', 6),
(9, '2018_11_08_143642_create_movie_genres_table', 7),
(10, '2018_11_08_143802_create_trailers_table', 8),
(11, '2018_11_08_143955_create_tv_series_table', 9),
(12, '2018_11_08_144155_create_comments_table', 10),
(13, '2018_11_08_144346_create_ratings_table', 11),
(14, '2018_11_08_144559_add_remaining_foriegn_keys', 12),
(15, '2018_11_08_145106_change_tv_series_series_i_d_type', 12),
(16, '2018_11_08_145852_add_remaining_foriegn_keys', 13),
(17, '2018_11_09_122846_add_fk_to_tv_series', 14),
(18, '2019_02_23_163050_add_profile_pic_to_user', 15),
(19, '2019_02_25_071908_create_favorites_table', 16),
(20, '2019_02_25_072712_add_foreign_keys_to_favorites', 17),
(21, '2019_02_26_095234_add_comment_field_in_comment', 18),
(22, '2019_02_26_111131_rename_user_i_d_in_comment', 19),
(23, '2019_06_28_161049_create_orders_table', 20),
(24, '2019_06_28_161713_add_f_k_to_orders', 21),
(25, '2019_06_28_161945_create_qualities_table', 22),
(26, '2019_06_28_162205_create_movie_qualities_table', 23),
(27, '2019_06_28_162451_add_f_k_to_movie_quality', 24),
(28, '2019_06_28_163007_create_order_details_table', 25),
(29, '2019_06_28_163208_add_f_k_to_order_detail', 26),
(30, '2019_06_28_163615_create_user_carts_table', 27),
(31, '2019_06_28_163746_add_f_k_to_user_cart', 28),
(32, '2019_07_01_134100_add_quanitity_to_user_cart', 29),
(33, '2019_07_01_150948_create_order_statuses_table', 30),
(34, '2019_07_01_151059_add_status_to_order', 31),
(35, '2019_07_01_151245_add_status_f_k_to_order', 32),
(36, '2019_07_06_095047_add_name_and_address_to_orders', 33),
(37, '2019_07_06_152243_add_is_admin_to_user', 34);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `synopsis` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `releaseDate` date NOT NULL,
  `length` time NOT NULL,
  `rating` double(8,2) NOT NULL,
  `isTvSeries` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`id`, `name`, `cover`, `thumbnail`, `synopsis`, `releaseDate`, `length`, `rating`, `isTvSeries`) VALUES
(11, 'Star Wars', 'https://image.tmdb.org/t/p/w1280//4iJfYYoQzZcONB9hNzg0J0wWyPH.jpg', 'https://image.tmdb.org/t/p/w500//btTdmkgIvOi0FFip1sPuZI2oQG6.jpg', 'Princess Leia is captured and held hostage by the evil Imperial forces in their effort to take over the galactic Empire. Venturesome Luke Skywalker and dashing captain Han Solo team together with the loveable robot duo R2-D2 and C-3PO to rescue the beautiful princess and restore peace and justice in the Empire.', '1977-05-25', '02:00:00', 8.20, 0),
(22, 'Pirates of the Caribbean: The Curse of the Black Pearl', 'https://image.tmdb.org/t/p/w1280//8AUQ7YlJJA9C8kWk8P4YNHIcFDE.jpg', 'https://image.tmdb.org/t/p/w500//tkt9xR1kNX5R9rCebASKck44si2.jpg', 'Jack Sparrow, a freewheeling 17th-century pirate, quarrels with a rival pirate bent on pillaging Port Royal. When the governor\'s daughter is kidnapped, Sparrow decides to help the girl\'s love save her.', '2003-07-09', '02:23:00', 8.00, 0),
(120, 'The Lord of the Rings: The Fellowship of the Ring', 'https://image.tmdb.org/t/p/w1280//pIUvQ9Ed35wlWhY2oU6OmwEsmzG.jpg', 'https://image.tmdb.org/t/p/w500//56zTpe2xvaA4alU51sRWPoKPYZy.jpg', 'Young hobbit Frodo Baggins, after inheriting a mysterious ring from his uncle Bilbo, must leave his home in order to keep it from falling into the hands of its evil creator. Along the way, a fellowship is formed to protect the ringbearer and make sure that the ring arrives at its final destination: Mt. Doom, the only place where it can be destroyed.', '2001-12-18', '02:58:00', 8.00, 0),
(456, 'The Simpsons', 'https://image.tmdb.org/t/p/w1280/lnnrirKFGwFW18GiH3AmuYy40cz.jpg', 'https://image.tmdb.org/t/p/w500/yTZQkSsxUFJZJe67IenRM0AEklc.jpg', 'Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general.', '1989-12-17', '00:00:00', 7.69, 1),
(671, 'Harry Potter and the Philosopher\'s Stone', 'https://image.tmdb.org/t/p/w1280//aPndWixw0hlH1XAXcSSZxW5LF8m.jpg', 'https://image.tmdb.org/t/p/w500//dCtFvscYcXQKTNvyyaQr2g2UacJ.jpg', 'Harry Potter has lived under the stairs at his aunt and uncle\'s house his whole life. But on his 11th birthday, he learns he\'s a powerful wizard -- with a place waiting for him at the Hogwarts School of Witchcraft and Wizardry. As he learns to harness his newfound powers with the help of the school\'s kindly headmaster, Harry uncovers the truth about his parents\' deaths -- and about the villain who\'s to blame.', '2001-11-16', '02:31:00', 8.00, 0),
(764, 'Midsomer Murders', 'https://image.tmdb.org/t/p/w1280//xtSVN9LYFdIS4CK34C61GC84ZH7.jpg', 'https://image.tmdb.org/t/p/w500//aOSoNRmQ39D8nxvX8vZOHQNQNDT.jpg', 'The peacefulness of the Midsomer community is shattered by violent crimes, suspects are placed under suspicion, and it is up to a veteran DCI and his young sergeant to calmly and diligently eliminate the innocent and ruthlessly pursue the guilty. ', '1997-03-23', '00:00:00', 8.00, 1),
(1396, 'Breaking Bad', 'https://image.tmdb.org/t/p/w1280//eSzpy96DwBujGFj0xMbXBcGcfxX.jpg', 'https://image.tmdb.org/t/p/w500//1yeVJox3rjo2jBKrrihIMj7uoS9.jpg', 'When Walter White, a New Mexico chemistry teacher, is diagnosed with Stage III cancer and given a prognosis of only two years left to live, he becomes filled with a sense of fearlessness and an unrelenting desire to secure his family\'s financial future at any cost as he enters the dangerous world of drugs and crime.', '2008-01-19', '00:00:00', 8.00, 1),
(1399, 'Game of Thrones', 'https://image.tmdb.org/t/p/w1280//gX8SYlnL9ZznfZwEH4KJUePBFUM.jpg', 'https://image.tmdb.org/t/p/w500//gwPSoYUHAKmdyVywgLpKKA4BjRr.jpg', 'Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night\'s Watch, is all that stands between the realms of men and icy horrors beyond.', '2011-04-17', '00:00:00', 8.00, 1),
(1402, 'The Walking Dead', 'https://image.tmdb.org/t/p/w1280//xVzvD5BPAU4HpleFSo8QOdHkndo.jpg', 'https://image.tmdb.org/t/p/w500//yn7psGTZsHumHOkLUmYpyrIcA2G.jpg', 'Sheriff\'s deputy Rick Grimes awakens from a coma to find a post-apocalyptic world dominated by flesh-eating zombies. He sets out to find his family and encounters many other survivors along the way.', '2010-10-31', '00:00:00', 7.00, 1),
(1403, 'Marvel\'s Agents of S.H.I.E.L.D.', 'https://image.tmdb.org/t/p/w1280//qtr5i6hOm6oVzTYl3jOQAYP3oc7.jpg', 'https://image.tmdb.org/t/p/w500//xjm6uVktPuKXNILwjLXwVG5d5BU.jpg', 'Agent Phil Coulson of S.H.I.E.L.D. (Strategic Homeland Intervention, Enforcement and Logistics Division) puts together a team of agents to investigate the new, the strange and the unknown around the globe, protecting the ordinary from the extraordinary.', '2013-09-24', '00:00:00', 7.00, 1),
(1412, 'Arrow', 'https://image.tmdb.org/t/p/w1280//dKxkwAJfGuznW8Hu0mhaDJtna0n.jpg', 'https://image.tmdb.org/t/p/w500//mo0FP1GxOFZT4UDde7RFDz5APXF.jpg', 'Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.', '2012-10-10', '00:00:00', 6.00, 1),
(1415, 'Elementary', 'https://image.tmdb.org/t/p/w1280//7sJrNKwzyJWnFPFpDL9wnZ859LZ.jpg', 'https://image.tmdb.org/t/p/w500//q9dObe29W4bDpgzUfOOH3ZnzDbR.jpg', 'A modern-day drama about a crime-solving duo that cracks the NYPD\'s most impossible cases. Following his fall from grace in London and a stint in rehab, eccentric Sherlock escapes to Manhattan where his wealthy father forces him to live with his worst nightmare - a sober companion, Dr. Watson.', '2012-09-27', '00:00:00', 7.00, 1),
(1416, 'Grey\'s Anatomy', 'https://image.tmdb.org/t/p/w1280//y6JABtgWMVYPx84Rvy7tROU5aNH.jpg', 'https://image.tmdb.org/t/p/w500//mgOZSS2FFIGtfVeac1buBw3Cx5w.jpg', 'Follows the personal and professional lives of a group of doctors at Seattleâ€™s Grey Sloan Memorial Hospital.', '2005-03-27', '00:00:00', 6.00, 1),
(1418, 'The Big Bang Theory', 'https://image.tmdb.org/t/p/w1280//nGsNruW3W27V6r4gkyc3iiEGsKR.jpg', 'https://image.tmdb.org/t/p/w500//ooBGRQBdbGzBxAVfExiO8r7kloA.jpg', 'The Big Bang Theory is centered on five characters living in Pasadena, California: roommates Leonard Hofstadter and Sheldon Cooper; Penny, a waitress and aspiring actress who lives across the hall; and Leonard and Sheldon\'s equally geeky and socially awkward friends and co-workers, mechanical engineer Howard Wolowitz and astrophysicist Raj Koothrappali. The geekiness and intellect of the four guys is contrasted for comic effect with Penny\'s social skills and common sense.', '2007-09-24', '00:00:00', 7.02, 1),
(1421, 'Modern Family', 'https://image.tmdb.org/t/p/w1280//snxFgPvHMcpW5f8Q3wu6uRRycww.jpg', 'https://image.tmdb.org/t/p/w500//gLAcu4VPCAb90oJvJ4nUJc5ZBQi.jpg', 'The Pritchett-Dunphy-Tucker clan is a wonderfully large and blended family. They give us an honest and often hilarious look into the sometimes warm, sometimes twisted, embrace of the modern family.', '2009-09-23', '00:00:00', 7.00, 1),
(1424, 'Orange Is the New Black', 'https://image.tmdb.org/t/p/w1280//tQfQjGwtXyczTCt0hGU0UnqA3NW.jpg', 'https://image.tmdb.org/t/p/w500//ekaa7YjGPTkFLcPhwWXTnARuCEU.jpg', 'A crime she committed in her youthful past sends Piper Chapman to a women\'s prison, where she trades her comfortable New York life for one of unexpected camaraderie and conflict in an eccentric group of fellow inmates.', '2013-07-11', '00:00:00', 7.00, 1),
(1431, 'CSI: Crime Scene Investigation', 'https://image.tmdb.org/t/p/w1280//nHaUMYpZ8qtLinqFXbgZoL9orHT.jpg', 'https://image.tmdb.org/t/p/w500//iUlElECnCViMIV8GnJwIM4A6Fss.jpg', 'The series follows Las Vegas criminalists working for the Las Vegas Police Department as they use physical evidence to solve grisly murders in this unusually graphic drama, which has inspired a host of other cop-show \"procedurals\". The series mixes deduction, gritty subject matter and character-driven drama. The network later added spin-offs CSI: Miami and CSI: NY, which have both been cancelled after ten and nine seasons respectively.', '2000-10-06', '00:00:00', 7.00, 1),
(1433, 'American Dad!', 'https://image.tmdb.org/t/p/w1280//vddYkC55H3DqWK5cG5hepSnhzGP.jpg', 'https://image.tmdb.org/t/p/w500//eo2Xu4UWXHE8UlBlAktNiSsAmfx.jpg', 'The series focuses on an eccentric motley crew that is the Smith family and their three housemates: Father, husband, and breadwinner Stan Smith; his better half housewife, Francine Smith; their college-aged daughter, Hayley Smith; and their high-school-aged son, Steve Smith. Outside of the Smith family, there are three additional main characters, including Hayley\'s boyfriend turned husband, Jeff Fischer; the family\'s man-in-a-goldfish-body pet, Klaus; and most notably the family\'s zany alien, Roger, who is \"full of masquerades, brazenness, and shocking antics.\"', '2005-02-06', '00:00:00', 6.00, 1),
(1434, 'Family Guy', 'https://image.tmdb.org/t/p/w1280//pH38r4TWTqq7Mcs6XAlwgzNUeJe.jpg', 'https://image.tmdb.org/t/p/w500//gBGUL1UTUNmdRQT8gA1LUV4yg39.jpg', 'Sick, twisted, politically incorrect and Freakin\' Sweet animated series featuring the adventures of the dysfunctional Griffin family. Bumbling Peter and long-suffering Lois have three kids. Stewie (a brilliant but sadistic baby bent on killing his mother and taking over the world), Meg (the oldest, and is the most unpopular girl in town) and Chris (the middle kid, he\'s not very bright but has a passion for movies). The final member of the family is Brian - a talking dog and much more than a pet, he keeps Stewie in check whilst sipping Martinis and sorting through his own life issues.', '1999-01-31', '00:00:00', 7.00, 1),
(1622, 'Supernatural', 'https://image.tmdb.org/t/p/w1280//koMUCyGWNtH5LXYbGqjsUwvgtsT.jpg', 'https://image.tmdb.org/t/p/w500//pui1V389cQft0BVFu9pbsYLEW1Q.jpg', 'When they were boys, Sam and Dean Winchester lost their mother to a mysterious and demonic supernatural force. Subsequently, their father raised them to be soldiers. He taught them about the paranormal evil that lives in the dark corners and on the back roads of America ... and he taught them how to kill it. Now, the Winchester brothers crisscross the country in their \'67 Chevy Impala, battling every kind of supernatural threat they encounter along the way. ', '2005-09-13', '00:00:00', 7.00, 1),
(1668, 'Friends', 'https://image.tmdb.org/t/p/w1280//efiX8iir6GEBWCD0uCFIi5NAyYA.jpg', 'https://image.tmdb.org/t/p/w500//7buCWBTpiPrCF5Lt023dSC60rgS.jpg', 'The misadventures of a group of friends as they navigate the pitfalls of work, life and love in Manhattan.', '1994-09-22', '00:00:00', 8.00, 1),
(1911, 'Bones', 'https://image.tmdb.org/t/p/w1280//8vv0ielb4e08E0z5E63aDW5MUp1.jpg', 'https://image.tmdb.org/t/p/w500//hYqFKoOZ54FOtL7r2WK6gQIMiQg.jpg', 'Dr. Temperance Brennan and herÂ colleagues at the Jeffersonian\'s Medico-Legal Lab assist Special Agent Seeley BoothÂ with murder investigations when the remains are so badly decomposed, burned or destroyed that the standard identification methods are useless.', '2005-09-12', '00:00:00', 7.00, 1),
(2046, 'Alias', 'https://image.tmdb.org/t/p/w1280//uqu5SuPIr0VQPqXF7CuGY3j2431.jpg', 'https://image.tmdb.org/t/p/w500//bMj6iM7TBcceG44vpireA2Axeab.jpg', 'Sydney Bristow, an agent who has been tricked to believe she is working for the U.S. government, is actually working for a criminal organization named the Alliance of Twelve. Upon learning this, Sydney becomes a double agent for the real CIA.', '2001-09-30', '00:00:00', 6.00, 1),
(2190, 'South Park', 'https://image.tmdb.org/t/p/w1280//mSDKNVvDfitFE6Fb6fSSl5DQmgS.jpg', 'https://image.tmdb.org/t/p/w500//v9zc0cZpy5aPSfAy6Tgb6I1zWgV.jpg', 'Follows the misadventures of four irreverent grade-schoolers in the quiet, dysfunctional town of South Park, Colorado.', '1997-08-13', '00:00:00', 8.00, 1),
(2691, 'Two and a Half Men', 'https://image.tmdb.org/t/p/w1280//27cshLy9anL9G0g6tBQjWkPACx5.jpg', 'https://image.tmdb.org/t/p/w500//A9QDK4OWpv41W27kCv0LXe30k9S.jpg', 'A hedonistic jingle writer\'s free-wheeling life comes to an abrupt halt when his brother and 10-year-old nephew move into his beach-front house.', '2003-09-22', '00:00:00', 6.00, 1),
(2734, 'Law & Order: Special Victims Unit', 'https://image.tmdb.org/t/p/w1280//kOvt2BOOwSAQCT8yo3pM3X2GXjh.jpg', 'https://image.tmdb.org/t/p/w500//yzMQBlirydvKp4Zgr5FbXlsrRmw.jpg', 'In the criminal justice system, sexually-based offenses are considered especially heinous. In New York City, the dedicated detectives who investigate these vicious felonies are members of an elite squad known as the Special Victims Unit. These are their stories.', '1999-09-20', '00:00:00', 6.00, 1),
(4057, 'Criminal Minds', 'https://image.tmdb.org/t/p/w1280//mzyLxbwy5jOveyfsbC1OWGp78NV.jpg', 'https://image.tmdb.org/t/p/w500//decs76l3VZiOtZ8BNp7KPPj2vcw.jpg', 'An elite team of FBI profilers analyze the country\'s most twisted criminal minds, anticipating their next moves before they strike again. The Behavioral Analysis Unit\'s most experienced agent is David Rossi, a founding member of the BAU who returns to help the team solve new cases.', '2005-09-22', '00:00:00', 7.00, 1),
(4614, 'NCIS', 'https://image.tmdb.org/t/p/w1280//nymeWHYQ1JaWP2wyNW5a5WHiDCd.jpg', 'https://image.tmdb.org/t/p/w500//1ubAPydzsb9VzhqeUGGDA7DZCUy.jpg', 'From murder and espionage to terrorism and stolen submarines, a team of special agents investigates any crime that has a shred of evidence connected to Navy and Marine Corps personnel, regardless of rank or position.', '2005-03-17', '00:00:00', 7.00, 1),
(5559, 'Bee Movie', 'https://image.tmdb.org/t/p/w1280//2lBpQZevIJdL4vPrZ7Nnm7mrVlm.jpg', 'https://image.tmdb.org/t/p/w500//QhbOzpYfBOwrRiuTu9otPk919R.jpg', 'Barry B. Benson, a bee who has just graduated from college, is disillusioned at his lone career choice: making honey. On a special trip outside the hive, Barry\'s life is saved by Vanessa, a florist in New York City. As their relationship blossoms, he discovers humans actually eat honey, and subsequently decides to sue us.', '2007-10-28', '01:30:00', 6.00, 0),
(8467, 'Dumb and Dumber', 'https://image.tmdb.org/t/p/w1280//2BkcMxgEq7i5x69Q6w77JeEewWr.jpg', 'https://image.tmdb.org/t/p/w500//3PEAkZHa8ehfUkuKbzmQNRTTAAs.jpg', 'Lloyd and Harry are two men whose stupidity is really indescribable. When Mary, a beautiful woman, loses an important suitcase with money before she leaves for Aspen, the two friends (who have found the suitcase) decide to return it to her. After some \"adventures\" they finally get to Aspen where, using the lost money they live it up and fight for Mary\'s heart.', '1994-12-16', '01:47:00', 7.00, 0),
(10195, 'Thor', 'https://image.tmdb.org/t/p/w1280//LvmmDZxkTDqp0DX7mUo621ahdX.jpg', 'https://image.tmdb.org/t/p/w500//bIuOWTtyFPjsFDevqvF3QrD1aun.jpg', 'Against his father Odin\'s will, The Mighty Thor - a powerful but arrogant warrior god - recklessly reignites an ancient war. Thor is cast down to Earth and forced to live among humans as punishment. Once here, Thor learns what it takes to be a true hero when the most dangerous villain of his world sends the darkest forces of Asgard to invade Earth.', '2011-04-21', '01:55:00', 7.00, 0),
(11683, 'Land of the Dead', 'https://image.tmdb.org/t/p/w1280//auPv1B8wVHnDn9ypAxeit44CFMN.jpg', 'https://image.tmdb.org/t/p/w500//1VYvkSiyJg9Za2R5KSHjUygLKEq.jpg', 'The world is full of zombies and the survivors have barricaded themselves inside a walled city to keep out the living dead. As the wealthy hide out in skyscrapers and chaos rules the streets, the rest of the survivors must find a way to stop the evolving zombies from breaking into the city.', '2005-06-24', '01:33:00', 6.00, 0),
(17610, 'NCIS: Los Angeles', 'https://image.tmdb.org/t/p/w1280//dQ8xZxtZtZTq74RjTycO1RKoqi0.jpg', 'https://image.tmdb.org/t/p/w500//YuY3IbeLnTYxJJ3PruZ6gl3LqK.jpg', 'The exploits of the Los Angelesâ€“based Office of Special Projects (OSP), an elite division of the Naval Criminal Investigative Service that specializes in undercover assignments.', '2009-09-22', '00:00:00', 7.00, 1),
(18165, 'The Vampire Diaries', 'https://image.tmdb.org/t/p/w1280//3O4r2guye3WwLBNPTfpfecU1FBn.jpg', 'https://image.tmdb.org/t/p/w500//vfF7IiyTar1ngfsd6n13LgFn9MD.jpg', 'The story of two vampire brothers obsessed with the same girl, who bears a striking resemblance to the beautiful but ruthless vampire they knew and loved in 1864.', '2009-09-10', '00:00:00', 7.00, 1),
(24428, 'The Avengers', 'https://image.tmdb.org/t/p/w1280//hbn46fQaRmlpBuUrEiFqv0GDL6Y.jpg', 'https://image.tmdb.org/t/p/w500//cezWGskPY5x7GaglTTRN4Fugfb8.jpg', 'When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!', '2012-04-25', '02:23:00', 7.00, 0),
(31917, 'Pretty Little Liars', 'https://image.tmdb.org/t/p/w1280//rQGBjWNveVeF8f2PGRtS85w9o9r.jpg', 'https://image.tmdb.org/t/p/w500//aUPbHiLS3hCHKjtLsncFa9g0viV.jpg', 'Based on the Pretty Little Liars series of young adult novels by Sara Shepard, the series follows the lives of four girls â€” Spencer, Hanna, Aria, and Emily â€” whose clique falls apart after the disappearance of their queen bee, Alison. One year later, they begin receiving messages from someone using the name \"A\" who threatens to expose their secrets â€” including long-hidden ones they thought only Alison knew.', '2010-06-08', '00:00:00', 6.00, 1),
(32798, 'Hawaii Five-0', 'https://image.tmdb.org/t/p/w1280//mAXpHFDTMHvJt7WdibFWdbRsdCG.jpg', 'https://image.tmdb.org/t/p/w500//y8QtBv7nyS2Auo0PNNgX9Hrh1Uz.jpg', 'Steve McGarrett returns home to Oahu, in order to find his father\'s killer. The governor offers him the chance to run his own task force (Five-0). Steve\'s team is joined by Chin Ho Kelly, Danny \"Danno\" Williams, and Kono Kalakaua.', '2010-09-20', '00:00:00', 6.00, 1),
(34307, 'Shameless', 'https://image.tmdb.org/t/p/w1280//zjOj2gnDJYFdYt6R7FtuHn7yrPr.jpg', 'https://image.tmdb.org/t/p/w500//iRyQTp2L437k6zfFCvZSOWaxQFA.jpg', 'Chicagoan Frank Gallagher is the proud single dad of six smart, industrious, independent kids, who without him would be... perhaps better off. When Frank\'s not at the bar spending what little money they have, he\'s passed out on the floor. But the kids have found ways to grow up in spite of him. They may not be like any family you know, but they make no apologies for being exactly who they are.', '2011-01-09', '00:00:00', 8.00, 1),
(34524, 'Teen Wolf', 'https://image.tmdb.org/t/p/w1280//swHKfpG1vj4fuBtix8jUc16ao9z.jpg', 'https://image.tmdb.org/t/p/w500//vY2vfAskJTiWsQSv6bdbNCQhPLm.jpg', 'Scott McCall, a high school student living in the town of Beacon Hills has his life drastically changed when he\'s bitten by a werewolf, becoming one himself. He must henceforth learn to balance his problematic new identity with his day-to-day teenage life. The following characters are instrumental to his struggle: Stiles, his best friend; Allison, his love interest who comes from a family of werewolf hunters; and Derek, a mysterious werewolf with a dark past. Throughout the series, he strives to keep his loved ones safe while maintaining normal relationships with them.', '2011-06-05', '00:00:00', 7.00, 1),
(37680, 'Suits', 'https://image.tmdb.org/t/p/w1280//7gRvLvHH78YAdchOghNpH2EysJ2.jpg', 'https://image.tmdb.org/t/p/w500//nVz7cZZ59PsCLgiWFC0M0N6AFC3.jpg', 'While running from a drug deal gone bad, Mike Ross, a brilliant young college-dropout, slips into a job interview with one of New York City\'s best legal closers, Harvey Specter. Tired of cookie-cutter law school grads, Harvey takes a gamble by hiring Mike on the spot after he recognizes his raw talent and photographic memory. Mike and Harvey are a winning team. Even though Mike is a genius, he still has a lot to learn about law. And while Harvey may seem like an emotionless, cold-blooded shark, Mike\'s sympathy and concern for their cases and clients will help remind Harvey why he went into law in the first place. Mike\'s other allies in the office include the firm\'s best paralegal Rachel and Harvey\'s no-nonsense assistant Donna to help him serve justice. Proving to be an irrepressible duo and invaluable to the practice, Mike and Harvey must keep their secret from everyone including managing partner Jessica and Harvey\'s arch nemesis Louis, who seems intent on making Mike\'s life as difficult as possible.', '2011-06-23', '00:00:00', 8.00, 1),
(39272, 'Once Upon a Time', 'https://image.tmdb.org/t/p/w1280//Otzq1sny6BAuvZdAO1EMdUkDUc.jpg', 'https://image.tmdb.org/t/p/w500//49qD372jeHUTmdNMGJkjCFZdv9y.jpg', 'There is a town in Maine where every story book character you\'ve ever known is trapped between two worlds, victims of a powerful curse. Only one knows the truth and only one can break the spell.\n\nEmma Swan is a 28-year-old bail bonds collector who has been supporting herself since she was abandoned as a baby. Things change for her when her son Henry, whom she abandoned years ago, finds her and asks for her help explaining that she is from a different world where she is Snow White\'s missing daughter.', '2011-10-23', '00:00:00', 6.00, 1),
(44217, 'Vikings', 'https://image.tmdb.org/t/p/w1280//A30ZqEoDbchvE7mCZcSp6TEwB1Q.jpg', 'https://image.tmdb.org/t/p/w500//oktTNFM8PzdseiK1X0E0XhB6LvP.jpg', 'Vikings follows the adventures of Ragnar Lothbrok, the greatest hero of his age. The series tells the sagas of Ragnar\'s band of Viking brothers and his family, as he rises to become King of the Viking tribes. As well as being a fearless warrior, Ragnar embodies the Norse traditions of devotion to the gods. Legend has it that he was a direct descendant of Odin, the god of war and warriors.', '2013-03-03', '00:00:00', 8.00, 1),
(44953, 'The Lord of the Skies', 'https://image.tmdb.org/t/p/w1280//qsTlD6rYb5vXOlBTT9Lj2hBp44X.jpg', 'https://image.tmdb.org/t/p/w500//bGnkIvPsnDquv9EE11S7PTvSeZm.jpg', 'Set in the 1990s, these are the life and times of Amado Carrillo Fuentes, a man who became the head of the JuÃ¡rez cartel. Nicknamed â€œEl SeÃ±or de los Cielosâ€ (Lord of the Skies) because of the large fleet of airplanes he used to transport drugs, he was also known for washing more than $200 million through Colombia to finance his huge fleet. He is described as the most powerful drug trafficker of his time.', '2013-04-15', '00:00:00', 5.00, 1),
(46896, 'The Originals', 'https://image.tmdb.org/t/p/w1280//7crHhmTybBvGQ3Y96j4kdQ89ldY.jpg', 'https://image.tmdb.org/t/p/w500//2llbXc2BOkLkBGgcNJCRbrWedUO.jpg', 'A spin-off from The Vampire Diaries and set in New Orleans, The Originals centers on the Mikaelson siblings, otherwise known as the world\'s original vampires: Klaus, Elijah, and Rebekah. Now Klaus must take down his protÃ©gÃ©, Marcel, who is now in charge of New Orleans, in order to re-take his city, as he originally built New Orleans. Klaus departed from the city after being chased down by his father Mikael, while it was being constructed and Marcel took charge. As Klaus has returned after many years, his ego has provoked him to become the king of the city. \"Every King needs an heir\" says Klaus, accepting the unborn child. The child is a first to be born to a hybrid and a werewolf.', '2013-10-03', '00:00:00', 7.00, 1),
(46952, 'The Blacklist', 'https://image.tmdb.org/t/p/w1280//fHwiAqIKragaCbNJo9Qs4lrccIN.jpg', 'https://image.tmdb.org/t/p/w500//bgbQCW4fE9b6wSOSC6Fb4FfVzsW.jpg', 'Raymond \"Red\" Reddington, one of the FBI\'s most wanted fugitives, surrenders in person at FBI Headquarters in Washington, D.C. He claims that he and the FBI have the same interests: bringing down dangerous criminals and terrorists. In the last two decades, he\'s made a list of criminals and terrorists that matter the most but the FBI cannot find because it does not know they exist. Reddington calls this \"The Blacklist\". Reddington will co-operate, but insists that he will speak only to Elizabeth Keen, a rookie FBI profiler.', '2013-09-23', '00:00:00', 7.00, 1),
(48866, 'The 100', 'https://image.tmdb.org/t/p/w1280//qYTIuJJ7fIehicAt3bl0vW70Sq6.jpg', 'https://image.tmdb.org/t/p/w500//wHIMMLFsk32wIzDmawWkYVbxFCS.jpg', 'Based on the books by Kass Morgan, this show takes place 100 years in the future, when the Earth has been abandoned due to radioactivity. The last surviving humans live on an ark orbiting the planet â€” but the ark won\'t last forever. So the repressive regime picks 100 expendable juvenile delinquents to send down to Earth to see if the planet is still habitable.', '2014-03-19', '00:00:00', 6.88, 1),
(57243, 'Doctor Who', 'https://image.tmdb.org/t/p/w1280//tQkigP2fItdzJWvtIhBvHxgs5yE.jpg', 'https://image.tmdb.org/t/p/w500//3EcYZhBMAvVw4czcDLg9Sd0FuzQ.jpg', 'The Doctor looks and seems human. He\'s handsome, witty, and could be mistaken for just another man in the street. But he is a Time Lord: a 900 year old alien with 2 hearts, part of a gifted civilization who mastered time travel. The Doctor saves planets for a living â€“ more of a hobby actually, and he\'s very, very good at it. He\'s saved us from alien menaces and evil from before time began â€“ but just who is he?', '2005-03-26', '00:00:00', 7.00, 1),
(60572, 'PokÃ©mon', 'https://image.tmdb.org/t/p/w1280//uxBFkJNwp9DEIE7k6YhKAorVpkp.jpg', 'https://image.tmdb.org/t/p/w500//2pcTUs20ysCdA6DZclaPmGXD6ph.jpg', 'Join Ash Ketchum, accompanied by his partner Pikachu, as he travels through many regions, meets new friends and faces new challenges on his quest to become a PokÃ©mon Master.', '1997-04-01', '00:00:00', 6.00, 1),
(60708, 'Gotham', 'https://image.tmdb.org/t/p/w1280//mKBP1OCgCG0jw8DwVYlnYqVILtc.jpg', 'https://image.tmdb.org/t/p/w500//5tSHzkJ1HBnyGdcpr6wSyw7jYnJ.jpg', 'Before there was Batman, there was GOTHAM. \n\nEveryone knows the name Commissioner Gordon. He is one of the crime world\'s greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon\'s story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world\'s most iconic villains? And what circumstances created them â€“ the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker? ', '2014-09-22', '00:00:00', 7.00, 1),
(60735, 'The Flash', 'https://image.tmdb.org/t/p/w1280//mmxxEpTqVdwBlu5Pii7tbedBkPC.jpg', 'https://image.tmdb.org/t/p/w500//lUFK7ElGCk9kVEryDJHICeNdmd1.jpg', 'After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won\'t be long before the world learns what Barry Allen has become...The Flash.', '2014-10-07', '00:00:00', 7.00, 1),
(61555, 'The Missing', 'https://image.tmdb.org/t/p/w1280//2hF2RnjVwHvxfhH2lkB9H9FdQHb.jpg', 'https://image.tmdb.org/t/p/w500//w1FDeOGoFS1qsSRHlj2Jzp2P0e1.jpg', 'A gripping anthological relationship thriller series exploring the emotional fallout of a child\'s abduction not only on the family but on the wider community, told over two time frames.', '2014-10-28', '00:00:00', 7.00, 1),
(62127, 'Marvel\'s Iron Fist', 'https://image.tmdb.org/t/p/w1280//xHCfWGlxwbtMeeOnTvxUCZRGnkk.jpg', 'https://image.tmdb.org/t/p/w500//nv4nLXbDhcISPP8C1mgaxKU50KO.jpg', 'Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.', '2017-03-17', '00:00:00', 6.00, 1),
(62688, 'Supergirl', 'https://image.tmdb.org/t/p/w1280//2qou2R47XZ1N6SlqGZcoCHDyEhN.jpg', 'https://image.tmdb.org/t/p/w500//ufoGrTRbItHvqk42yNHcyoE0afM.jpg', 'Twenty-four-year-old Kara Zor-El, who was taken in by the Danvers family when she was 13 after being sent away from Krypton, must learn to embrace her powers after previously hiding them. The Danvers teach her to be careful with her powers, until she has to reveal them during an unexpected disaster, setting her on her journey of heroism.', '2015-10-26', '00:00:00', 6.00, 1),
(63174, 'Lucifer', 'https://image.tmdb.org/t/p/w1280//6kZCuvbirx6ibJfaWqwV2p90yPI.jpg', 'https://image.tmdb.org/t/p/w500//wmOuYyqVaczBLXxQNFSaRfAUgPz.jpg', 'Bored and unhappy as the Lord of Hell, Lucifer Morningstar abandoned his throne and retired to Los Angeles, where he has teamed up with LAPD detective Chloe Decker to take down criminals.Â But the longer he\'s away from the underworld, the greater the threat that the worst of humanity could escape.', '2016-01-25', '00:00:00', 7.00, 1),
(63247, 'Westworld', 'https://image.tmdb.org/t/p/w1280//yp94aOXzuqcQHva90B3jxLfnOO9.jpg', 'https://image.tmdb.org/t/p/w500//6aj09UTMQNyfSfk0ZX8rYOEsXL2.jpg', 'A dark odyssey about the dawn of artificial consciousness and the evolution of sin. Set at the intersection of the near future and the reimagined past, it explores a world in which every human appetite, no matter how noble or depraved, can be indulged.', '2016-10-02', '00:00:00', 8.00, 1),
(63639, 'The Expanse', 'https://image.tmdb.org/t/p/w1280//beIjmWr3OBOtcWK4tKMObOIDJ1C.jpg', 'https://image.tmdb.org/t/p/w500//prJFWxJ0x8tBPTliMjj51wgYnSK.jpg', 'A thriller set two hundred years in the future following the case of a missing young woman who brings a hardened detective and a rogue ship\'s captain together in a race across the solar system to expose the greatest conspiracy in human history.', '2015-12-14', '00:00:00', 8.00, 1),
(66857, 'Shooter', 'https://image.tmdb.org/t/p/w1280//wLq6GiuzpmLozY6EfPzWdocB2bg.jpg', 'https://image.tmdb.org/t/p/w500//oMmqtafm6Rdlka1ejJeC9BDWzyO.jpg', 'Bob Lee Swagger is an expert marksman living in exile who is coaxed back into action after learning of a plot to kill the president. Based on the best-selling Bob Lee Swagger novel by Stephen Hunter, Point of Impact, and the 2007 Paramount film starring Mark Wahlberg.', '2016-11-15', '00:00:00', 7.00, 1),
(67744, 'Mindhunter', 'https://image.tmdb.org/t/p/w1280//a906PH7CDmSOdS7kmnAgdWk5mhv.jpg', 'https://image.tmdb.org/t/p/w500//r7RIwuceOaDP4KTmU1EFeDniRq4.jpg', 'An agent in the FBI\'s Elite Serial Crime Unit develops profiling techniques as he pursues notorious serial killers and rapists.', '2017-10-13', '00:00:00', 7.00, 1),
(69050, 'Riverdale', 'https://image.tmdb.org/t/p/w1280//2IUpoKSP64r6rp2vBo0Fdk8a1UU.jpg', 'https://image.tmdb.org/t/p/w500//1TsbOTztAJtzTRXAhoLsX9a83XX.jpg', 'Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath Riverdaleâ€™s wholesome facade.', '2017-01-26', '00:00:00', 7.00, 1),
(69478, 'The Handmaid\'s Tale', 'https://image.tmdb.org/t/p/w1280//1lgss4ZTOsFgY0p9xRf4hl85BNR.jpg', 'https://image.tmdb.org/t/p/w500//tELIZw0ugIxqjBKrhWbROlgCbz4.jpg', 'Set in a dystopian future, a woman is forced to live as a concubine under a fundamentalist theocratic dictatorship. A TV adaptation of Margaret Atwood\'s novel.', '2017-04-26', '00:00:00', 8.00, 1),
(70513, 'Salvation', 'https://image.tmdb.org/t/p/w1280//iWatl2zNjHbG1mDpzJIKCsNxDgr.jpg', 'https://image.tmdb.org/t/p/w500//hXCd9FUT7ZjoB390cGioVhEiXOB.jpg', 'An MIT grad student and a tech superstar bring a low-level Pentagon official a staggering discovery that an asteroid is just six months away from colliding with Earth.', '2017-07-12', '00:00:00', 7.00, 1),
(71446, 'Money Heist', 'https://image.tmdb.org/t/p/w1280//flyWT9eIE8vdy1tzEGxBN4RkoeN.jpg', 'https://image.tmdb.org/t/p/w500//2d0ESa56OesIFAOAl8ZZi0mHlxC.jpg', 'To carry out the biggest heist in history, a mysterious man called The Professor recruits a band of eight robbers who have a single characteristic: none of them has anything to lose. Five months of seclusion - memorizing every step, every detail, every probability - culminate in eleven days locked up in the National Coinage and Stamp Factory of Spain, surrounded by police forces and with dozens of hostages in their power, to find out whether their suicide wager will lead to everything or nothing.', '2017-05-02', '00:00:00', 8.00, 1),
(100042, 'Dumb and Dumber To', 'https://image.tmdb.org/t/p/w1280//pMNSiNT55XrxWPmoI5ytJyinRa8.jpg', 'https://image.tmdb.org/t/p/w500//Ekw3ijq9L6RiKvv5m2tPOEklHF.jpg', '20 years after the dimwits set out on their first adventure, they head out in search of one of their long lost children in the hope of gaining a new kidney.', '2014-11-12', '01:49:00', 5.00, 0),
(102651, 'Maleficent', 'https://image.tmdb.org/t/p/w1280//tO4xw0P4mdia3EcS6pHkox3cOIy.jpg', 'https://image.tmdb.org/t/p/w500//4FYu1AAu14tuBU0lns0hbKiHUcH.jpg', 'The untold story of Disney\'s most iconic villain from the 1959 classic \'Sleeping Beauty\'. A beautiful, pure-hearted young woman, Maleficent has an idyllic life growing up in a peaceable forest kingdom, until one day when an invading army threatens the harmony of the land.  Maleficent rises to be the land\'s fiercest protector, but she ultimately suffers a ruthless betrayal â€“ an act that begins to turn her heart into stone. Bent on revenge, Maleficent faces an epic battle with the invading King\'s successor and, as a result, places a curse upon his newborn infant Aurora. As the child grows, Maleficent realizes that Aurora holds the key to peace in the kingdom - and to Maleficent\'s true happiness as well.', '2014-05-28', '01:37:00', 7.00, 0),
(102899, 'Ant-Man', 'https://image.tmdb.org/t/p/w1280//kvXLZqY0Ngl1XSw7EaMQO0C1CCj.jpg', 'https://image.tmdb.org/t/p/w500//D6e8RJf2qUstnfkTslTXNTUAlT.jpg', 'Armed with the astonishing ability to shrink in scale but increase in strength, master thief Scott Lang must embrace his inner-hero and help his mentor, Doctor Hank Pym, protect the secret behind his spectacular Ant-Man suit from a new generation of towering threats. Against seemingly insurmountable obstacles, Pym and Lang must plan and pull off a heist that will save the world.', '2015-07-14', '01:57:00', 7.00, 0),
(118340, 'Guardians of the Galaxy', 'https://image.tmdb.org/t/p/w1280//bHarw8xrmQeqf3t8HpuMY7zoK4x.jpg', 'https://image.tmdb.org/t/p/w500//y31QB9kn3XSudA15tV7UWQ9XLuW.jpg', 'Light years from Earth, 26 years after being abducted, Peter Quill finds himself the prime target of a manhunt after discovering an orb wanted by Ronan the Accuser.', '2014-07-30', '02:00:00', 8.00, 0),
(135397, 'Jurassic World', 'https://image.tmdb.org/t/p/w1280//t5KONotASgVKq4N19RyhIthWOPG.jpg', 'https://image.tmdb.org/t/p/w500//jjBgi2r5cRt36xF6iNUEhzscEcb.jpg', 'Twenty-two years after the events of Jurassic Park, Isla Nublar now features a fully functioning dinosaur theme park, Jurassic World, as originally envisioned by John Hammond.', '2015-06-06', '02:04:00', 7.00, 0),
(141052, 'Justice League', 'https://image.tmdb.org/t/p/w1280//o5T8rZxoWSBMYwjsUFUqTt6uMQB.jpg', 'https://image.tmdb.org/t/p/w500//eifGNCSDuxJeS1loAXil5bIGgvC.jpg', 'Fuelled by his restored faith in humanity and inspired by Superman\'s selfless act, Bruce Wayne and Diana Prince assemble a team of metahumans consisting of Barry Allen, Arthur Curry and Victor Stone to face the catastrophic threat of Steppenwolf and the Parademons who are on the hunt for three Mother Boxes on Earth.', '2017-11-15', '02:00:00', 6.00, 0),
(157336, 'Interstellar', 'https://image.tmdb.org/t/p/w1280//xu9zaAevzQ5nnrsXN6JcahLnG4i.jpg', 'https://image.tmdb.org/t/p/w500//nBNZadXqJSdt05SHLqgT0HuC5Gm.jpg', 'Interstellar chronicles the adventures of a group of explorers who make use of a newly discovered wormhole to surpass the limitations on human space travel and conquer the vast distances involved in an interstellar voyage.', '2014-11-05', '02:49:00', 8.44, 0),
(157350, 'Divergent', 'https://image.tmdb.org/t/p/w1280//g6WT9zxATzTy9NVu2xwbxDAxvjd.jpg', 'https://image.tmdb.org/t/p/w500//yTtx2ciqk4XdN1oKhMMDy3f5ue3.jpg', 'In a world divided into factions based on personality types, Tris learns that she\'s been classified as Divergent and won\'t fit in. When she discovers a plot to destroy Divergents, Tris and the mysterious Four must find out what makes Divergents dangerous before it\'s too late.', '2014-03-14', '02:19:00', 7.00, 0),
(159824, 'Hotel Transylvania 2', 'https://image.tmdb.org/t/p/w1280//h3t4zZPteUPqyDBrmz5Z2zzGM61.jpg', 'https://image.tmdb.org/t/p/w500//t4PLWexbe4wbNydCOBv18cYexup.jpg', 'When the old-old-old-fashioned vampire Vlad arrives at the hotel for an impromptu family get-together, Hotel Transylvania is in for a collision of supernatural old-school and modern day cool.', '2015-09-21', '01:29:00', 7.00, 0),
(180894, 'Ninja: Shadow of a Tear', 'https://image.tmdb.org/t/p/w1280//qkhjHfXxZKD317GeifrepN9YJSt.jpg', 'https://image.tmdb.org/t/p/w500//k5uuo4mvWHb80ePDZylN2xvcz7h.jpg', 'Fight everyone and trust no one: it\'s the code of survival practiced by martial-arts master Casey Bowman after his life of domestic bliss is shattered by a savage act of violence. Vowing revenge, the fearless American stealthily tracks the killer from Osaka to Bangkok to Rangoon with the help of a wise and crafty sensei. His only clues: a series of victims whose necks bear the distinctive mark of strangulation by barbed wire. Fighting to avenge as well as to survive, Casey must sharpen his razor-like responses and take his battle skills to the next level, even using deep meditation to fake his own death. His target: the sinister drug lord Goro, who is flooding the streets with deadly meth cooked at his remote jungle factory. To prepare for his ultimate confrontation, Casey must finally become an invisible warrior worthy of the name Ninja. But just when his prey is cornered, an unexpected twist shows Casey that his battle is only beginning: he truly can trust no one.', '2013-12-27', '01:34:00', 6.00, 0),
(181808, 'Star Wars: The Last Jedi', 'https://image.tmdb.org/t/p/w1280//oVdLj5JVqNWGY0LEhBfHUuMrvWJ.jpg', 'https://image.tmdb.org/t/p/w500//kOVEVeg59E0wsnXmF9nrh6OmWII.jpg', 'Rey develops her newly discovered abilities with the guidance of Luke Skywalker, who is unsettled by the strength of her powers. Meanwhile, the Resistance prepares to do battle with the First Order.', '2017-12-13', '02:31:00', 7.00, 0),
(198663, 'The Maze Runner', 'https://image.tmdb.org/t/p/w1280//lkOZcsXcOLZYeJ2YxJd3vSldvU4.jpg', 'https://image.tmdb.org/t/p/w500//coss7RgL0NH6g4fC2s5atvf3dFO.jpg', 'Set in a post-apocalyptic world, young Thomas is deposited in a community of boys after his memory is erased, soon learning they\'re all trapped in a maze that will require him to join forces with fellow â€œrunnersâ€ for a shot at escape.', '2014-09-10', '01:53:00', 7.03, 0),
(209112, 'Batman v Superman: Dawn of Justice', 'https://image.tmdb.org/t/p/w1280//vsjBeMPZtyB7yNsYY56XYxifaQZ.jpg', 'https://image.tmdb.org/t/p/w500//cGOPbv9wA5gEejkUN892JrveARt.jpg', 'Fearing the actions of a god-like Super Hero left unchecked, Gotham Cityâ€™s own formidable, forceful vigilante takes on Metropolisâ€™s most revered, modern-day savior, while the world wrestles with what sort of hero it really needs. And with Batman and Superman at war with one another, a new threat quickly arises, putting mankind in greater danger than itâ€™s ever known before.', '2016-03-23', '02:30:00', 6.00, 0),
(241259, 'Alice Through the Looking Glass', 'https://image.tmdb.org/t/p/w1280//fAIIEyPuO6eJmXqb3XVXEx6SNjn.jpg', 'https://image.tmdb.org/t/p/w500//7rfucKCjV01OEsGr9grXa34Aywd.jpg', 'In the sequel to Tim Burton\'s \"Alice in Wonderland\", Alice Kingsleigh returns to Underland and faces a new adventure in saving the Mad Hatter.', '2016-05-25', '01:53:00', 7.00, 0),
(263115, 'Logan', 'https://image.tmdb.org/t/p/w1280//5pAGnkFYSsFJ99ZxDIYnhQbQFXs.jpg', 'https://image.tmdb.org/t/p/w500//gGBu0hKw9BGddG8RkRAMX7B6NDB.jpg', 'In the near future, a weary Logan cares for an ailing Professor X in a hideout on the Mexican border. But Logan\'s attempts to hide from the world and his legacy are upended when a young mutant arrives, pursued by dark forces.', '2017-02-28', '02:16:00', 8.00, 0),
(268896, 'Pacific Rim: Uprising', 'https://image.tmdb.org/t/p/w1280//6pT73ACl5N1VekdK3wQI8PLfz1E.jpg', 'https://image.tmdb.org/t/p/w500//v5HlmJK9bdeHxN2QhaFP1ivjX3U.jpg', 'It has been ten years since The Battle of the Breach and the oceans are still, but restless. Vindicated by the victory at the Breach, the Jaeger program has evolved into the most powerful global defense force in human history. The PPDC now calls upon the best and brightest to rise up and become the next generation of heroes when the Kaiju threat returns.', '2018-03-21', '01:51:00', 6.00, 0),
(274855, 'Geostorm', 'https://image.tmdb.org/t/p/w1280//79X8JkGxzc1tJMi0KxUSaPLooVg.jpg', 'https://image.tmdb.org/t/p/w500//nrsx0jEaBgXq4PWo7SooSnYJTv.jpg', 'After an unprecedented series of natural disasters threatened the planet, the world\'s leaders came together to create an intricate network of satellites to control the global climate and keep everyone safe. But now, something has gone wrong: the system built to protect Earth is attacking it, and it becomes a race against the clock to uncover the real threat before a worldwide geostorm wipes out everything and everyone along with it.', '2017-10-13', '01:49:00', 6.00, 0),
(281338, 'War for the Planet of the Apes', 'https://image.tmdb.org/t/p/w1280//ulMscezy9YX0bhknvJbZoUgQxO5.jpg', 'https://image.tmdb.org/t/p/w500//oDvgC7IeTdc03SgiSSo2HY7xGYX.jpg', 'Caesar and his apes are forced into a deadly conflict with an army of humans led by a ruthless Colonel. After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind. As the journey finally brings them face to face, Caesar and the Colonel are pitted against each other in an epic battle that will determine the fate of both their species and the future of the planet.', '2017-07-11', '02:20:00', 7.00, 0),
(283995, 'Guardians of the Galaxy Vol. 2', 'https://image.tmdb.org/t/p/w1280//aJn9XeesqsrSLKcHfHP4u5985hn.jpg', 'https://image.tmdb.org/t/p/w500//y4MBh0EjBlMuOzv9axM4qJlmhzz.jpg', 'The Guardians must fight to keep their newfound family together as they unravel the mysteries of Peter Quill\'s true parentage.', '2017-04-19', '02:16:00', 8.00, 0),
(284054, 'Black Panther', 'https://image.tmdb.org/t/p/w1280//b6ZJZHUdMEFECvGiDpJjlfUWela.jpg', 'https://image.tmdb.org/t/p/w500//uxzzxijgPIY7slzFvMotPv8wjKA.jpg', 'King T\'Challa returns home from America to the reclusive, technologically advanced African nation of Wakanda to serve as his country\'s new leader. However, T\'Challa soon finds that he is challenged for the throne by factions within his own country as well as without. Using powers reserved to Wakandan kings, T\'Challa assumes the Black Panther mantel to join with girlfriend Nakia, the queen-mother, his princess-kid sister, members of the Dora Milaje (the Wakandan \'special forces\') and an American secret agent, to prevent Wakanda from being dragged into a world war.', '2018-02-13', '02:14:00', 7.00, 0),
(297762, 'Wonder Woman', 'https://image.tmdb.org/t/p/w1280//6iUNJZymJBMXXriQyFZfLAKnjO6.jpg', 'https://image.tmdb.org/t/p/w500//imekS7f1OuHyUP2LAiTEM0zBzUz.jpg', 'An Amazon princess comes to the world of Man in the grips of the First World War to confront the forces of evil and bring an end to human conflict.', '2017-05-30', '02:21:00', 7.00, 0),
(316727, 'The Purge: Election Year', 'https://image.tmdb.org/t/p/w1280//lPhsKTtycmbp6bzJAG662Pd5U3N.jpg', 'https://image.tmdb.org/t/p/w500//bjOJFjUffRZQpNQ9g3yvc9F6LBM.jpg', 'Two years after choosing not to kill the man who killed his son, former police sergeant Leo Barnes has become head of security for Senator Charlene Roan, the front runner in the next Presidential election due to her vow to eliminate the Purge. On the night of what should be the final Purge, a betrayal from within the government forces Barnes and Roan out onto the street where they must fight to survive the night.', '2016-06-29', '01:45:00', 6.00, 0),
(335984, 'Blade Runner 2049', 'https://image.tmdb.org/t/p/w1280//mVr0UiqyltcfqxbAUcLl9zWL8ah.jpg', 'https://image.tmdb.org/t/p/w500//gajva2L0rPYkEWjzgFlBXCAVBE5.jpg', 'Thirty years after the events of the first film, a new blade runner, LAPD Officer K, unearths a long-buried secret that has the potential to plunge what\'s left of society into chaos. K\'s discovery leads him on a quest to find Rick Deckard, a former LAPD blade runner who has been missing for 30 years.', '2017-10-04', '02:43:00', 7.00, 0),
(336843, 'Maze Runner: The Death Cure', 'https://image.tmdb.org/t/p/w1280//bvbyidkMaBls1LTaIWYY6UmYTaL.jpg', 'https://image.tmdb.org/t/p/w500//7GgZ6DGezkh3szFdvskH5XD4V0t.jpg', 'Thomas leads his group of escaped Gladers on their final and most dangerous mission yet. To save their friends, they must break into the legendary Last City, a WCKD-controlled labyrinth that may turn out to be the deadliest maze of all. Anyone who makes it out alive will get answers to the questions the Gladers have been asking since they first arrived in the maze.', '2018-01-10', '02:23:00', 7.00, 0),
(337339, 'The Fate of the Furious', 'https://image.tmdb.org/t/p/w1280//jzdnhRhG0dsuYorwvSqPqqnM1cV.jpg', 'https://image.tmdb.org/t/p/w500//dImWM7GJqryWJO9LHa3XQ8DD5NH.jpg', 'When a mysterious woman seduces Dom into the world of crime and a betrayal of those closest to him, the crew face trials that will test them as never before.', '2017-04-12', '02:15:00', 7.00, 0),
(338970, 'Tomb Raider', 'https://image.tmdb.org/t/p/w1280//bLJTjfbZ1c5zSNiAvGYs1Uc82ir.jpg', 'https://image.tmdb.org/t/p/w500//3zrC5tUiR35rTz9stuIxnU1nUS5.jpg', 'Lara Croft, the fiercely independent daughter of a missing adventurer, must push herself beyond her limits when she finds herself on the island where her father disappeared.', '2018-03-05', '01:57:00', 6.00, 0),
(340022, 'Gringo', 'https://image.tmdb.org/t/p/w1280//1u2f04uoVGTjmThK71yDDqeQwK0.jpg', 'https://image.tmdb.org/t/p/w500//dxeVPklFwng2IowqRPNI4od6aXv.jpg', 'An American businessman with a stake in a pharmaceutical company that\'s about to go public finds his life is thrown into turmoil by an incident in Mexico.', '2018-03-08', '01:49:00', 6.00, 0),
(345887, 'The Equalizer 2', 'https://image.tmdb.org/t/p/w1280//a24i4Qw6qFTtkFLQsncv8WPwevj.jpg', 'https://image.tmdb.org/t/p/w500//cQvc9N6JiMVKqol3wcYrGshsIdZ.jpg', 'Robert McCall returns to deliver his special brand of vigilante justice -- but how far will he go when it\'s someone he loves?', '2018-07-19', '02:00:00', 9.00, 0),
(347882, 'Sleight', 'https://image.tmdb.org/t/p/w1280//2SEgJ0mHJ7TSdVDbkGU061tR33K.jpg', 'https://image.tmdb.org/t/p/w500//wridRvGxDqGldhzAIh3IcZhHT5F.jpg', 'A young street magician is left to take care of his little sister after his mother\'s passing and turns to drug dealing in the Los Angeles party scene to keep a roof over their heads. When he gets into trouble with his supplier, his sister is kidnapped and he is forced to rely on both his sleight of hand and brilliant mind to save her.', '2017-04-28', '01:29:00', 5.00, 0),
(375098, 'The Show', 'https://image.tmdb.org/t/p/w1280//A4pEYA8zDqIHBf4nEHP9TbPzTou.jpg', 'https://image.tmdb.org/t/p/w500//oe0WzIcsCfwYLu5WhHKOjXO4aYx.jpg', 'An unsettling look at reality television, where a disturbing game show has its contestants ending their lives for the public\'s enjoyment.', '2017-09-21', '01:44:00', 6.00, 0),
(381288, 'Split', 'https://image.tmdb.org/t/p/w1280//4G6FNNLSIVrwSRZyFs91hQ3lZtD.jpg', 'https://image.tmdb.org/t/p/w500//rXMWOZiCt6eMX22jWuTOSdQ98bY.jpg', 'Though Kevin has evidenced 23 personalities to his trusted psychiatrist, Dr. Fletcher, there remains one still submerged who is set to materialize and dominate all the others. Compelled to abduct three teenage girls led by the willful, observant Casey, Kevin reaches a war for survival among all of those contained within him â€” as well as everyone around him â€” as the walls between his compartments shatter apart.', '2017-01-19', '01:57:00', 7.00, 0),
(396371, 'Molly\'s Game', 'https://image.tmdb.org/t/p/w1280//yvbXGWjg30sj7rohEZvSe90jSJC.jpg', 'https://image.tmdb.org/t/p/w500//h9hUP5ZJGsjL2wbERrGlj4dMjZq.jpg', 'Molly Bloom, a young skier and former Olympic hopeful becomes a successful entrepreneur (and a target of an FBI investigation) when she establishes a high-stakes, international poker game.', '2017-12-21', '02:20:00', 7.00, 0),
(399055, 'The Shape of Water', 'https://image.tmdb.org/t/p/w1280//hVYhrKuQNFro6jXHZMn60uYjrIP.jpg', 'https://image.tmdb.org/t/p/w500//k4FwHlMhuRR5BISY2Gm2QZHlH5Q.jpg', 'An other-worldly story, set against the backdrop of Cold War era America circa 1962, where a mute janitor working at a lab falls in love with an amphibious man being held captive there and devises a plan to help him escape.', '2017-12-01', '02:02:00', 7.00, 0);
INSERT INTO `movies` (`id`, `name`, `cover`, `thumbnail`, `synopsis`, `releaseDate`, `length`, `rating`, `isTvSeries`) VALUES
(400155, 'Hotel Transylvania 3: Summer Vacation', 'https://image.tmdb.org/t/p/w1280//m03jul0YdVEOFXEQVUv6pOVQYGL.jpg', 'https://image.tmdb.org/t/p/w500//gjAFM4xhA5vyLxxKMz38ujlUfDL.jpg', 'Dracula, Mavis, Johnny and the rest of the Drac Pack take a vacation on a luxury Monster Cruise Ship, where Dracula falls in love with the ship\'s captain, Ericka, who\'s secretly a descendant of Abraham Van Helsing, the notorious monster slayer.', '2018-06-30', '01:37:00', 7.00, 0),
(400579, 'Tully', 'https://image.tmdb.org/t/p/w1280//h4Z6IUpTyD9ViV7NLmbAEbgM1G0.jpg', 'https://image.tmdb.org/t/p/w500//wDI4YXBXolMYi15Qx2kClvdSERM.jpg', 'Marlo, a mother of three including a newborn, is gifted a night nanny by her brother. Hesitant to the extravagance at first, Marlo comes to form a bond with the thoughtful, surprising, and sometimes challenging nanny named Tully.', '2018-05-04', '01:36:00', 7.00, 0),
(401981, 'Red Sparrow', 'https://image.tmdb.org/t/p/w1280//sGzuQYSTwJvLBc2PnuSVLHhuFeh.jpg', 'https://image.tmdb.org/t/p/w500//vLCogyfQGxVLDC1gqUdNAIkc29L.jpg', 'Prima ballerina, Dominika Egorova faces a bleak and uncertain future after she suffers an injury that ends her career. She soon turns to Sparrow School, a secret intelligence service that trains exceptional young people to use their minds and bodies as weapons. Dominika emerges as the most dangerous Sparrow after completing the sadistic training process. As she comes to terms with her new abilities, she meets a CIA agent who tries to convince her that he is the only person she can trust.', '2018-02-28', '02:20:00', 6.00, 0),
(406990, 'What Happened to Monday', 'https://image.tmdb.org/t/p/w1280//wwZ2uXOwPkMrZSeFn9s7WFXEMg6.jpg', 'https://image.tmdb.org/t/p/w500//o6EsOqITcSzcdwD1zxBM9imdxjr.jpg', 'In a world where families are limited to one child due to overpopulation, a set of identical septuplets must avoid being put to a long sleep by the government and dangerous infighting while investigating the disappearance of one of their own.', '2017-08-18', '02:02:00', 7.00, 0),
(427641, 'Rampage', 'https://image.tmdb.org/t/p/w1280//wrqUiMXttHE4UBFMhLHlN601MZh.jpg', 'https://image.tmdb.org/t/p/w500//3gIO6mCd4Q4PF1tuwcyI3sjFrtI.jpg', 'Primatologist Davis Okoye shares an unshakable bond with George, the extraordinarily intelligent, silverback gorilla who has been in his care since birth.  But a rogue genetic experiment gone awry mutates this gentle ape into a raging creature of enormous size.  To make matters worse, itâ€™s soon discovered there are other similarly altered animals.  As these newly created alpha predators tear across North America, destroying everything in their path, Okoye teams with a discredited genetic engineer to secure an antidote, fighting his way through an ever-changing battlefield, not only to halt a global catastrophe but to save the fearsome creature that was once his friend.', '2018-04-12', '01:47:00', 8.00, 0),
(429417, 'Ghost Stories', 'https://image.tmdb.org/t/p/w1280//7vyzEMfcEpBChEyK7cP6XW6B7hi.jpg', 'https://image.tmdb.org/t/p/w500//7a0sai9k5jr7YJRP5S3YIDErMVY.jpg', 'Arch sceptic Professor Phillip Goodman embarks upon a terror filled quest when he stumbles across a long-lost file containing details of three cases of inexplicable \'hauntings\'.', '2018-04-06', '01:38:00', 7.00, 0),
(437033, 'Toc Toc', 'https://image.tmdb.org/t/p/w1280//oA4n3fCEW86QoLTDDlAfik9eOq9.jpg', 'https://image.tmdb.org/t/p/w500//1D44oDrIVPSFzCB6Kb87pIfdWBz.jpg', 'Toc Toc is a 2017 Spanish comedy film directed by Vicente Villanueva. The movie follows the adventures and misadventures of a group of patients with OCD dated at the same time.', '2017-09-01', '01:30:00', 7.00, 0),
(447200, 'Skyscraper', 'https://image.tmdb.org/t/p/w1280//oMKFQmoVgB69fyXfSMu0lGlHJP2.jpg', 'https://image.tmdb.org/t/p/w500//3NL1FiTEhU4SWOuWk8lkVCbmnYm.jpg', 'Framed and on the run, a former FBI agent must save his family from a blazing fire in the world\'s tallest building.', '2018-07-11', '01:42:00', 6.00, 0),
(447332, 'A Quiet Place', 'https://image.tmdb.org/t/p/w1280//tZmr0ozxSc2GasRuddtlRkf7f68.jpg', 'https://image.tmdb.org/t/p/w500//nAU74GmpUk7t5iklEp3bufwDq4n.jpg', 'A family is forced to live in silence while hiding from creatures that hunt by sound.', '2018-04-03', '01:30:00', 7.00, 0),
(454619, 'Overboard', 'https://image.tmdb.org/t/p/w1280//1c9UOyJP0FnhhdNoEvhI3ODcE3i.jpg', 'https://image.tmdb.org/t/p/w500//qauHzSQJdo9VYX4NKn9PdWnvXOV.jpg', 'A spoiled, wealthy yacht owner is thrown overboard and becomes the target of revenge from his mistreated employee. A remake of the 1987 comedy.', '2018-05-03', '01:52:00', 6.00, 0),
(454983, 'The Kissing Booth', 'https://image.tmdb.org/t/p/w1280//xEKx7zPEjN6meomZ7OhV82Mm2jm.jpg', 'https://image.tmdb.org/t/p/w500//7Dktk2ST6aL8h9Oe5rpk903VLhx.jpg', 'When teenager Elle\'s first kiss leads to a forbidden romance with the hottest boy in high school, she risks her relationship with her best friend.', '2018-05-11', '01:45:00', 8.00, 0),
(458423, 'Mamma Mia! Here We Go Again', 'https://image.tmdb.org/t/p/w1280//8HKwxHZ23sW8wz4yb28IWnAM1M6.jpg', 'https://image.tmdb.org/t/p/w500//aWicerX4Y7n7tUwRAVHsVcBBpj2.jpg', 'On the Greek island of Kalokairi, the daughter of Donna Sheridan, Sophie, finds out more about her mother\'s past while seeking guidance on how to handle her pregnancy.', '2018-07-18', '02:00:00', 8.00, 0),
(470878, 'I Can Only Imagine', 'https://image.tmdb.org/t/p/w1280//9XjNaS2S0RJfjbUPDAzILPprU3r.jpg', 'https://image.tmdb.org/t/p/w500//veZszwMZu8d3WMU6TJX9sV5w1Y4.jpg', 'Growing up in Texas, Bart Millard suffers physical and emotional abuse at the hands of his father. His childhood and relationship with his dad inspires him to write the hit song \"I Can Only Imagine\" as singer of the Christian band MercyMe.', '2018-03-08', '01:49:00', 7.00, 0),
(476292, 'Maquia: When the Promised Flower Blooms', 'https://image.tmdb.org/t/p/w1280//cfbjFQ14hSTgXChBEvaEjFiUaKb.jpg', 'https://image.tmdb.org/t/p/w500//vOrK1n6VgVCFBwgcq5tMoPCqa47.jpg', 'A story of encounters and partings interwoven between people; this is a human drama with feelings that touch one\'s heart gradually, which everyone has experienced at least once.', '2018-02-24', '01:55:00', 6.00, 0),
(492719, 'Godzilla: City on the Edge of Battle', 'https://image.tmdb.org/t/p/w1280//mu4q1jPLdW5hE7BWZEWRvuAh3ZN.jpg', 'https://image.tmdb.org/t/p/w500//hF85hujJ4jelKtlSZ8YdthvjdZt.jpg', 'Humanity\'s desperate battle to reclaim the Earth from Godzilla continues. The key to defeating the King of the Monsters may be Mechagodzilla, a robotic weapon thought to have been lost nearly 20,000 years ago.', '2018-05-18', '01:45:00', 6.00, 0),
(493006, 'Detective Conan: Zero the Enforcer', 'https://image.tmdb.org/t/p/w1280//kEqeponciiz6TyuKWtnKSzXzbGa.jpg', 'https://image.tmdb.org/t/p/w500//mMWV5MXn2pkDnnI4vWpy3dRWdNC.jpg', 'There is a sudden explosion at Tokyo Summit\'s giant Edge of Ocean facility. The shadow of TÅru Amuro, who works for the National Police Agency Security Bureau as Zero, appears at the site. In addition, the \"triple-face\" character is known as Rei Furuya as a detective and KogorÅ MÅri\'s apprentice, and he is also known as Bourbon as a Black Organization member. KogorÅ is arrested as a suspect in the case of the explosion. Conan conducts an investigation to prove KogorÅ\'s innocence, but Amuro gets in his way.', '2018-04-13', '01:55:00', 5.00, 0),
(493007, 'Shaft', 'https://image.tmdb.org/t/p/original/iBW3nRykmJNPcyeZikaMUDtRisR.jpg', 'https://image.tmdb.org/t/p/w500/kfZqwGuvEBAysAbCsa0QLKoSYR.jpg', 'JJ, aka John Shaft Jr., may be a cyber security expert with a degree from MIT, but to uncover the truth behind his best friend’s untimely death, he needs an education only his dad can provide. Absent throughout JJ’s youth, the legendary locked-and-loaded John Shaft agrees to help his progeny navigate Harlem’s heroin-infested underbelly.', '2019-06-14', '01:51:00', 8.00, 0),
(493008, 'Spider-Man: Far from Home', 'https://image.tmdb.org/t/p/original/hSzmNczQBnYjbd5rhShdfmPwqti.jpg', 'https://image.tmdb.org/t/p/original/rjbNpRMoVvqHmhmksbokcyCr7wn.jpg', 'Peter Parker and his friends go on a summer trip to Europe. However, they will hardly be able to rest - Peter will have to agree to help Nick Fury uncover the mystery of creatures that cause natural disasters and destruction throughout the continent.', '2019-06-16', '02:09:00', 8.10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `movie_genres`
--

CREATE TABLE `movie_genres` (
  `movie_id` int(10) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movie_genres`
--

INSERT INTO `movie_genres` (`movie_id`, `id`) VALUES
(427641, 28),
(427641, 12),
(427641, 878),
(427641, 14),
(284054, 28),
(284054, 12),
(284054, 14),
(284054, 878),
(447332, 18),
(447332, 27),
(447332, 53),
(447332, 878),
(159824, 16),
(159824, 35),
(159824, 10751),
(159824, 14),
(476292, 16),
(429417, 18),
(429417, 27),
(492719, 28),
(492719, 878),
(492719, 12),
(492719, 16),
(400579, 35),
(400579, 18),
(493006, 16),
(493006, 80),
(493006, 9648),
(493006, 28),
(493006, 18),
(102651, 14),
(102651, 12),
(102651, 28),
(102651, 10751),
(102651, 10749),
(181808, 14),
(181808, 12),
(181808, 878),
(181808, 28),
(671, 12),
(671, 14),
(671, 10751),
(437033, 35),
(118340, 28),
(118340, 878),
(118340, 12),
(268896, 28),
(268896, 14),
(268896, 878),
(268896, 12),
(141052, 28),
(141052, 12),
(141052, 14),
(141052, 878),
(447200, 28),
(447200, 53),
(447200, 18),
(338970, 28),
(338970, 12),
(338970, 14),
(338970, 18),
(338970, 9648),
(338970, 53),
(157350, 28),
(157350, 12),
(157350, 878),
(316727, 28),
(316727, 27),
(316727, 53),
(102899, 878),
(102899, 28),
(102899, 12),
(281338, 18),
(281338, 878),
(281338, 10752),
(470878, 18),
(470878, 10751),
(470878, 10402),
(400155, 10751),
(400155, 14),
(400155, 35),
(400155, 16),
(335984, 9648),
(335984, 878),
(335984, 53),
(406990, 878),
(406990, 53),
(406990, 18),
(135397, 28),
(135397, 12),
(135397, 878),
(135397, 53),
(263115, 28),
(263115, 18),
(263115, 878),
(345887, 53),
(345887, 28),
(345887, 80),
(396371, 80),
(396371, 18),
(241259, 12),
(241259, 10751),
(241259, 14),
(375098, 18),
(399055, 18),
(399055, 14),
(399055, 10749),
(24428, 878),
(24428, 28),
(24428, 12),
(381288, 27),
(381288, 53),
(11683, 27),
(11683, 878),
(454983, 10749),
(454983, 35),
(120, 12),
(120, 14),
(120, 28),
(5559, 10751),
(5559, 16),
(5559, 12),
(5559, 35),
(198663, 28),
(198663, 9648),
(198663, 878),
(198663, 53),
(454619, 10749),
(454619, 35),
(180894, 28),
(180894, 80),
(180894, 53),
(347882, 18),
(347882, 53),
(347882, 28),
(347882, 878),
(209112, 28),
(209112, 12),
(209112, 14),
(100042, 35),
(458423, 35),
(458423, 10749),
(458423, 10402),
(297762, 28),
(297762, 12),
(297762, 14),
(297762, 10752),
(297762, 878),
(274855, 28),
(274855, 878),
(274855, 53),
(8467, 35),
(336843, 28),
(336843, 9648),
(336843, 878),
(336843, 53),
(336843, 12),
(336843, 14),
(157336, 12),
(157336, 18),
(157336, 878),
(340022, 35),
(340022, 28),
(340022, 80),
(11, 12),
(11, 28),
(11, 878),
(22, 12),
(22, 14),
(22, 28),
(10195, 12),
(10195, 14),
(10195, 28),
(401981, 9648),
(401981, 53),
(337339, 12),
(337339, 28),
(337339, 80),
(337339, 53),
(337339, 9648),
(283995, 28),
(283995, 12),
(283995, 35),
(283995, 878),
(1418, 35),
(48866, 18),
(48866, 10765),
(67744, 80),
(67744, 18),
(1402, 18),
(1402, 10759),
(1402, 10765),
(456, 16),
(456, 35),
(1622, 18),
(1622, 9648),
(1622, 10765),
(2734, 80),
(2734, 18),
(1399, 18),
(1399, 10759),
(1399, 10765),
(61555, 18),
(61555, 9648),
(46896, 18),
(46896, 9648),
(46896, 10765),
(1668, 35),
(57243, 18),
(57243, 10759),
(57243, 10765),
(1434, 16),
(1434, 35),
(1416, 18),
(60735, 18),
(60735, 10765),
(70513, 18),
(70513, 10765),
(1412, 28),
(1412, 80),
(1412, 18),
(1412, 9648),
(1403, 18),
(1403, 10759),
(1403, 10765),
(46952, 18),
(46952, 80),
(46952, 9648),
(1911, 80),
(1911, 18),
(60572, 10759),
(60572, 16),
(2046, 10759),
(2046, 18),
(60708, 18),
(60708, 14),
(60708, 80),
(2190, 35),
(2190, 16),
(2691, 35),
(62127, 10759),
(62127, 80),
(62127, 18),
(62127, 10765),
(764, 80),
(764, 18),
(764, 9648),
(62688, 28),
(62688, 12),
(62688, 18),
(62688, 878),
(4057, 80),
(4057, 18),
(4057, 9648),
(1396, 18),
(63174, 80),
(63174, 10765),
(4614, 10759),
(4614, 80),
(4614, 18),
(63247, 37),
(63247, 878),
(17610, 10759),
(17610, 18),
(17610, 80),
(17610, 9648),
(63639, 18),
(63639, 9648),
(63639, 10765),
(18165, 18),
(18165, 14),
(18165, 27),
(18165, 10749),
(66857, 10759),
(31917, 18),
(31917, 9648),
(32798, 28),
(32798, 80),
(32798, 18),
(1415, 18),
(1415, 9648),
(1415, 80),
(34307, 18),
(34307, 35),
(69478, 18),
(69478, 10765),
(34524, 10759),
(34524, 9648),
(34524, 10765),
(34524, 18),
(37680, 18),
(1421, 35),
(71446, 80),
(71446, 18),
(39272, 18),
(39272, 10765),
(1424, 35),
(1424, 18),
(44217, 10759),
(44217, 18),
(1431, 80),
(1431, 18),
(1431, 9648),
(44953, 80),
(44953, 18),
(44953, 10766),
(1433, 16),
(1433, 35),
(69050, 18),
(69050, 9648),
(493007, 80),
(493007, 28),
(493008, 28),
(493008, 12),
(493008, 878);

-- --------------------------------------------------------

--
-- Table structure for table `movie_qualities`
--

CREATE TABLE `movie_qualities` (
  `id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `movie_qualities`
--

INSERT INTO `movie_qualities` (`id`, `movie_id`) VALUES
(1, 11),
(2, 11),
(3, 11),
(1, 22),
(2, 22),
(3, 22),
(1, 120),
(2, 120),
(3, 120),
(1, 456),
(2, 456),
(3, 456),
(1, 671),
(2, 671),
(3, 671),
(1, 764),
(2, 764),
(3, 764),
(1, 1396),
(2, 1396),
(3, 1396),
(1, 1399),
(2, 1399),
(3, 1399),
(1, 1402),
(2, 1402),
(3, 1402),
(1, 1403),
(2, 1403),
(3, 1403),
(1, 1412),
(2, 1412),
(3, 1412),
(1, 1415),
(2, 1415),
(3, 1415),
(1, 1416),
(2, 1416),
(3, 1416),
(1, 1418),
(2, 1418),
(3, 1418),
(1, 1421),
(2, 1421),
(3, 1421),
(1, 1424),
(2, 1424),
(3, 1424),
(1, 1431),
(2, 1431),
(3, 1431),
(1, 1433),
(2, 1433),
(3, 1433),
(1, 1434),
(2, 1434),
(3, 1434),
(1, 1622),
(2, 1622),
(3, 1622),
(1, 1668),
(2, 1668),
(3, 1668),
(1, 1911),
(2, 1911),
(3, 1911),
(1, 2046),
(2, 2046),
(3, 2046),
(1, 2190),
(2, 2190),
(3, 2190),
(1, 2691),
(2, 2691),
(3, 2691),
(1, 2734),
(2, 2734),
(3, 2734),
(1, 4057),
(2, 4057),
(3, 4057),
(1, 4614),
(2, 4614),
(3, 4614),
(1, 5559),
(2, 5559),
(3, 5559),
(1, 8467),
(2, 8467),
(3, 8467),
(1, 10195),
(2, 10195),
(3, 10195),
(1, 11683),
(2, 11683),
(3, 11683),
(1, 17610),
(2, 17610),
(3, 17610),
(1, 18165),
(2, 18165),
(3, 18165),
(1, 24428),
(2, 24428),
(3, 24428),
(1, 31917),
(2, 31917),
(3, 31917),
(1, 32798),
(2, 32798),
(3, 32798),
(1, 34307),
(2, 34307),
(3, 34307),
(1, 34524),
(2, 34524),
(3, 34524),
(1, 37680),
(2, 37680),
(3, 37680),
(1, 39272),
(2, 39272),
(3, 39272),
(1, 44217),
(2, 44217),
(3, 44217),
(1, 44953),
(2, 44953),
(3, 44953),
(1, 46896),
(2, 46896),
(3, 46896),
(1, 46952),
(2, 46952),
(3, 46952),
(1, 48866),
(2, 48866),
(3, 48866),
(1, 57243),
(2, 57243),
(3, 57243),
(1, 60572),
(2, 60572),
(3, 60572),
(1, 60708),
(2, 60708),
(3, 60708),
(1, 60735),
(2, 60735),
(3, 60735),
(1, 61555),
(2, 61555),
(3, 61555),
(1, 62127),
(2, 62127),
(3, 62127),
(1, 62688),
(2, 62688),
(3, 62688),
(1, 63174),
(2, 63174),
(3, 63174),
(1, 63247),
(2, 63247),
(3, 63247),
(1, 63639),
(2, 63639),
(3, 63639),
(1, 66857),
(2, 66857),
(3, 66857),
(1, 67744),
(2, 67744),
(3, 67744),
(1, 69050),
(2, 69050),
(3, 69050),
(1, 69478),
(2, 69478),
(3, 69478),
(1, 70513),
(2, 70513),
(3, 70513),
(1, 71446),
(2, 71446),
(3, 71446),
(1, 100042),
(2, 100042),
(3, 100042),
(1, 102651),
(2, 102651),
(3, 102651),
(1, 102899),
(2, 102899),
(3, 102899),
(1, 118340),
(2, 118340),
(3, 118340),
(1, 135397),
(2, 135397),
(3, 135397),
(1, 141052),
(2, 141052),
(3, 141052),
(1, 157336),
(2, 157336),
(3, 157336),
(1, 157350),
(2, 157350),
(3, 157350),
(1, 159824),
(2, 159824),
(3, 159824),
(1, 180894),
(2, 180894),
(3, 180894),
(1, 181808),
(2, 181808),
(3, 181808),
(1, 198663),
(2, 198663),
(3, 198663),
(1, 209112),
(2, 209112),
(3, 209112),
(1, 241259),
(2, 241259),
(3, 241259),
(1, 263115),
(2, 263115),
(3, 263115),
(1, 268896),
(2, 268896),
(3, 268896),
(1, 274855),
(2, 274855),
(3, 274855),
(1, 281338),
(2, 281338),
(3, 281338),
(1, 283995),
(2, 283995),
(3, 283995),
(1, 284054),
(2, 284054),
(3, 284054),
(1, 297762),
(2, 297762),
(3, 297762),
(1, 316727),
(2, 316727),
(3, 316727),
(1, 335984),
(2, 335984),
(3, 335984),
(1, 336843),
(2, 336843),
(3, 336843),
(1, 337339),
(2, 337339),
(3, 337339),
(1, 338970),
(2, 338970),
(3, 338970),
(1, 340022),
(2, 340022),
(3, 340022),
(1, 345887),
(2, 345887),
(3, 345887),
(1, 347882),
(2, 347882),
(3, 347882),
(1, 375098),
(2, 375098),
(3, 375098),
(1, 381288),
(2, 381288),
(3, 381288),
(1, 396371),
(2, 396371),
(3, 396371),
(1, 399055),
(2, 399055),
(3, 399055),
(1, 400155),
(2, 400155),
(3, 400155),
(1, 400579),
(2, 400579),
(3, 400579),
(1, 401981),
(2, 401981),
(3, 401981),
(1, 406990),
(2, 406990),
(3, 406990),
(1, 427641),
(2, 427641),
(3, 427641),
(1, 429417),
(2, 429417),
(3, 429417),
(1, 437033),
(2, 437033),
(3, 437033),
(1, 447200),
(2, 447200),
(3, 447200),
(1, 447332),
(2, 447332),
(3, 447332),
(1, 454619),
(2, 454619),
(3, 454619),
(1, 454983),
(2, 454983),
(3, 454983),
(1, 458423),
(2, 458423),
(3, 458423),
(1, 470878),
(2, 470878),
(3, 470878),
(1, 476292),
(2, 476292),
(3, 476292),
(1, 492719),
(2, 492719),
(3, 492719),
(1, 493006),
(2, 493006),
(3, 493006),
(3, 493007),
(2, 493007),
(1, 493007),
(1, 493008),
(2, 493008),
(3, 493008);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) UNSIGNED NOT NULL,
  `receiver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `created_at`, `updated_at`, `status`, `receiver`) VALUES
(11, 16, '2019-04-09 05:44:39', '2019-07-06 09:42:51', 3, 'Ghayas'),
(12, 16, '2019-04-24 05:45:09', '2019-07-06 09:50:11', 3, 'Ghayas'),
(13, 16, '2019-07-06 05:45:21', '2019-07-06 09:58:42', 3, 'Ghayas'),
(14, 16, '2019-07-06 05:45:32', '2019-07-06 09:42:33', 2, 'Ghayas'),
(15, 16, '2019-03-20 05:45:43', '2019-07-06 10:06:25', 3, 'Ghayas'),
(18, 16, '2019-07-06 06:07:28', '2019-07-06 09:09:16', 3, 'Ghayas'),
(20, 16, '2019-02-21 06:07:46', '2019-07-06 09:08:33', 3, 'Ghayas'),
(22, 16, '2019-07-06 06:08:19', '2019-07-06 10:02:55', 3, 'Ghayas'),
(23, 16, '2019-02-13 06:08:27', '2019-07-06 09:48:58', 3, 'Ghayas'),
(24, 16, '2019-07-06 06:08:36', '2019-07-06 09:40:45', 3, 'Ghayas'),
(25, 26, '2019-07-06 10:41:14', '2019-07-06 10:45:40', 3, 'John Wick'),
(27, 26, '2019-07-06 10:46:35', '2019-07-06 10:46:35', 1, 'John Wick'),
(28, 26, '2019-07-06 10:46:43', '2019-07-06 15:05:09', 2, 'John Wick'),
(29, 16, '2019-07-06 10:47:01', '2019-07-06 10:47:01', 1, 'Ghayas'),
(30, 26, '2019-06-12 19:00:00', '2019-06-12 19:00:00', 3, 'John Wick'),
(31, 25, '2019-05-14 19:00:00', '2019-05-14 19:00:00', 3, 'Harry'),
(32, 26, '2019-06-17 19:00:00', '2019-06-17 19:00:00', 3, 'John Wick'),
(35, 16, '2019-07-08 12:18:11', '2019-07-08 12:18:11', 1, 'Ghayas'),
(36, 16, '2019-07-08 12:19:45', '2019-07-08 12:19:45', 1, 'Ghayas'),
(37, 16, '2019-07-08 12:21:55', '2019-07-08 12:21:55', 1, 'Ghayas'),
(38, 16, '2019-07-08 12:25:33', '2019-07-08 12:27:40', 3, 'Ghayas'),
(39, 16, '2019-07-08 12:46:51', '2019-07-08 12:46:51', 1, 'Ghayas');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(11) UNSIGNED NOT NULL,
  `movie_id` int(11) UNSIGNED NOT NULL,
  `quality_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `movie_id`, `quality_id`, `created_at`, `updated_at`) VALUES
(18, 11, 345887, 3, '2019-07-06 05:44:39', '2019-07-06 05:44:39'),
(19, 11, 493006, 1, '2019-07-06 05:44:39', '2019-07-06 05:44:39'),
(20, 12, 447332, 2, '2019-07-06 05:45:09', '2019-07-06 05:45:09'),
(21, 13, 671, 3, '2019-07-06 05:45:21', '2019-07-06 05:45:21'),
(22, 14, 427641, 3, '2019-07-06 05:45:32', '2019-07-06 05:45:32'),
(23, 15, 157336, 1, '2019-07-06 05:45:43', '2019-07-06 05:45:43'),
(27, 18, 458423, 3, '2019-07-06 06:07:28', '2019-07-06 06:07:28'),
(30, 20, 458423, 3, '2019-07-06 06:07:46', '2019-07-06 06:07:46'),
(31, 20, 429417, 3, '2019-07-06 06:07:46', '2019-07-06 06:07:46'),
(32, 20, 340022, 2, '2019-07-06 06:07:46', '2019-07-06 06:07:46'),
(34, 22, 338970, 2, '2019-07-06 06:08:19', '2019-07-06 06:08:19'),
(35, 22, 181808, 3, '2019-07-06 06:08:19', '2019-07-06 06:08:19'),
(36, 23, 338970, 2, '2019-07-06 06:08:27', '2019-07-06 06:08:27'),
(37, 23, 181808, 3, '2019-07-06 06:08:27', '2019-07-06 06:08:27'),
(38, 23, 335984, 3, '2019-07-06 06:08:27', '2019-07-06 06:08:27'),
(39, 24, 338970, 2, '2019-07-06 06:08:36', '2019-07-06 06:08:36'),
(40, 24, 181808, 3, '2019-07-06 06:08:36', '2019-07-06 06:08:36'),
(41, 24, 335984, 3, '2019-07-06 06:08:36', '2019-07-06 06:08:36'),
(42, 24, 437033, 2, '2019-07-06 06:08:37', '2019-07-06 06:08:37'),
(43, 25, 67744, 3, '2019-07-06 10:41:14', '2019-07-06 10:41:14'),
(46, 27, 406990, 2, '2019-07-06 10:46:35', '2019-07-06 10:46:35'),
(47, 27, 283995, 3, '2019-07-06 10:46:35', '2019-07-06 10:46:35'),
(48, 28, 406990, 2, '2019-07-06 10:46:43', '2019-07-06 10:46:43'),
(49, 28, 283995, 3, '2019-07-06 10:46:43', '2019-07-06 10:46:43'),
(50, 28, 281338, 1, '2019-07-06 10:46:43', '2019-07-06 10:46:43'),
(51, 29, 429417, 2, '2019-07-06 10:47:01', '2019-07-06 10:47:01'),
(52, 30, 102899, 2, '2019-05-20 19:00:00', '2019-04-18 19:00:00'),
(53, 31, 209112, 3, '2019-05-20 19:00:00', '2019-04-18 19:00:00'),
(54, 32, 1396, 3, '2019-05-20 19:00:00', '2019-04-18 19:00:00'),
(60, 35, 447200, 3, '2019-07-08 12:18:11', '2019-07-08 12:18:11'),
(61, 36, 427641, 3, '2019-07-08 12:19:45', '2019-07-08 12:19:45'),
(62, 36, 493006, 1, '2019-07-08 12:19:45', '2019-07-08 12:19:45'),
(63, 37, 492719, 2, '2019-07-08 12:21:56', '2019-07-08 12:21:56'),
(64, 38, 429417, 3, '2019-07-08 12:25:33', '2019-07-08 12:25:33'),
(65, 38, 400155, 2, '2019-07-08 12:25:34', '2019-07-08 12:25:34'),
(66, 38, 268896, 3, '2019-07-08 12:25:34', '2019-07-08 12:25:34'),
(67, 38, 447332, 1, '2019-07-08 12:25:34', '2019-07-08 12:25:34'),
(68, 38, 470878, 2, '2019-07-08 12:25:34', '2019-07-08 12:25:34'),
(69, 38, 336843, 3, '2019-07-08 12:25:34', '2019-07-08 12:25:34'),
(70, 39, 493008, 3, '2019-07-08 12:46:52', '2019-07-08 12:46:52');

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`) VALUES
(1, 'Processing'),
(2, 'Shipped'),
(3, 'Delivered');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `qualities`
--

CREATE TABLE `qualities` (
  `movie_quality_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `qualities`
--

INSERT INTO `qualities` (`movie_quality_id`, `type`, `price`) VALUES
(1, 'HD 1080p', 29.99),
(2, 'DVD', 19.99),
(3, 'Bluray', 59.99);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `userID` int(10) UNSIGNED NOT NULL,
  `movieID` int(10) UNSIGNED NOT NULL,
  `rating` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `trailers`
--

CREATE TABLE `trailers` (
  `id` int(10) UNSIGNED NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trailers`
--

INSERT INTO `trailers` (`id`, `link`, `movie_id`) VALUES
(5337, 'https://www.youtube.com/embed/DX1iplQQJTo', 456),
(5338, 'https://www.youtube.com/embed/vZ734NWnAHA', 11),
(5339, 'https://www.youtube.com/embed/i-vsILeJ8_8', 11),
(5340, 'https://www.youtube.com/embed/FFGaS6MDknY', 5559),
(5341, 'https://www.youtube.com/embed/uHBnrJowBZE', 10195),
(5342, 'https://www.youtube.com/embed/atXJB9luiko', 11683),
(5343, 'https://www.youtube.com/embed/RnddBOTuedc', 180894),
(5344, 'https://www.youtube.com/embed/2LIQ2-PZBC8', 118340),
(5345, 'https://www.youtube.com/embed/d96cjJhvlMA', 118340),
(5346, 'https://www.youtube.com/embed/9k5Z8pui9Us', 61555),
(5347, 'https://www.youtube.com/embed/bIqvQFqPavA', 100042),
(5348, 'https://www.youtube.com/embed/qo6hU98dXOc', 1412),
(5349, 'https://www.youtube.com/embed/RFinNxS5KN4', 135397),
(5350, 'https://www.youtube.com/embed/V75dMMIW2B4', 120),
(5351, 'https://www.youtube.com/embed/aStYWD25fAQ', 120),
(5352, 'https://www.youtube.com/embed/VzSsxINwIVE', 4614),
(5353, 'https://www.youtube.com/embed/Mx7xTF8fKz4', 60735),
(5354, 'https://www.youtube.com/embed/lGTOru7pwL8', 1668),
(5355, 'https://www.youtube.com/embed/FLYHRErz8Zo', 2190),
(5356, 'https://www.youtube.com/embed/nu9mslgDcR4', 34307),
(5357, 'https://www.youtube.com/embed/0bIAZz-xCGo', 1431),
(5358, 'https://www.youtube.com/embed/Or5YChRcVAM', 17610),
(5359, 'https://www.youtube.com/embed/y0j_hA6aKQU', 1415),
(5360, 'https://www.youtube.com/embed/T3T-evQZiQo', 1403),
(5361, 'https://www.youtube.com/embed/McOKQsgmoPc', 34524),
(5362, 'https://www.youtube.com/embed/aZxhA_Jihmw', 60735),
(5363, 'https://www.youtube.com/embed/hTv13EjlLNg', 1412),
(5364, 'https://www.youtube.com/embed/fBITGyJynfA', 1424),
(5365, 'https://www.youtube.com/embed/0d1zpt6k5OI', 60708),
(5366, 'https://www.youtube.com/embed/1rCYDbefnmA', 34524),
(5367, 'https://www.youtube.com/embed/-WYdUaK54fU', 46952),
(5368, 'https://www.youtube.com/embed/EN6LMV4-Dfs', 32798),
(5369, 'https://www.youtube.com/embed/1j2sXLbzm9U', 44217),
(5370, 'https://www.youtube.com/embed/ff-XiZzJLxw', 1415),
(5371, 'https://www.youtube.com/embed/R1v0uFms68U', 1402),
(5372, 'https://www.youtube.com/embed/Rga4rp4j5TY', 39272),
(5373, 'https://www.youtube.com/embed/CHstRRD_8LE', 34307),
(5374, 'https://www.youtube.com/embed/X4bF_quwNtw', 63174),
(5375, 'https://www.youtube.com/embed/DHyKECa1VaA', 1421),
(5377, 'https://www.youtube.com/embed/zLyUlTu4KbI', 1424),
(5378, 'https://www.youtube.com/embed/aJJrkyHas78', 135397),
(5379, 'https://www.youtube.com/embed/6p5BEzoGwD4', 57243),
(5380, 'https://www.youtube.com/embed/U7dLXjZfXV8', 1421),
(5381, 'https://www.youtube.com/embed/8X5gXIQmY-E', 63639),
(5382, 'https://www.youtube.com/embed/ydKmedH336Q', 63639),
(5383, 'https://www.youtube.com/embed/etSoOqk67UQ', 63639),
(5384, 'https://www.youtube.com/embed/RXMp9fBomJw', 316727),
(5385, 'https://www.youtube.com/embed/OiEG3Zr_Jxs', 241259),
(5386, 'https://www.youtube.com/embed/fomWLkyO_SY', 241259),
(5387, 'https://www.youtube.com/embed/bSgTU0svg9M', 241259),
(5388, 'https://www.youtube.com/embed/0xduIrmvnUE', 241259),
(5389, 'https://www.youtube.com/embed/f20pL_rn3DE', 241259),
(5390, 'https://www.youtube.com/embed/dX-bT1SdKj8', 241259),
(5391, 'https://www.youtube.com/embed/KKE0Py64jmk', 241259),
(5392, 'https://www.youtube.com/embed/Nfcn9Um8sk8', 102899),
(5393, 'https://www.youtube.com/embed/TfTcx_TwXq8', 102899),
(5394, 'https://www.youtube.com/embed/79mfp1hY3Uw', 102899),
(5395, 'https://www.youtube.com/embed/6as8ahAr1Uc', 209112),
(5396, 'https://www.youtube.com/embed/T3nqmGgnJe8', 159824),
(5397, 'https://www.youtube.com/embed/q_lgu-gskSg', 159824),
(5398, 'https://www.youtube.com/embed/I7NR9MLK5W4', 159824),
(5399, 'https://www.youtube.com/embed/OIx8OGeEh9c', 159824),
(5400, 'https://www.youtube.com/embed/IUUMODHe7r8', 159824),
(5401, 'https://www.youtube.com/embed/w-XO4XiRop0', 102651),
(5402, 'https://www.youtube.com/embed/704EXbJ-b5k', 102651),
(5403, 'https://www.youtube.com/embed/c1sS3fLOJFg', 102651),
(5404, 'https://www.youtube.com/embed/_pgmFAOgm5E', 102651),
(5405, 'https://www.youtube.com/embed/b7yOuhI1dzU', 118340),
(5406, 'https://www.youtube.com/embed/3CqymRQ1uUU', 118340),
(5407, 'https://www.youtube.com/embed/64-iSYVmMVY', 198663),
(5408, 'https://www.youtube.com/embed/3b946aGm0zs', 198663),
(5409, 'https://www.youtube.com/embed/apr31MFjg08', 1433),
(5410, 'https://www.youtube.com/embed/aDrsItJ_HU4', 48866),
(5411, 'https://www.youtube.com/embed/XZ8daibM3AE', 1396),
(5412, 'https://www.youtube.com/embed/krqqqgixNq8', 63639),
(5413, 'https://www.youtube.com/embed/X2x6IR3FhbQ', 66857),
(5414, 'https://www.youtube.com/embed/nyc6RJEEe0U', 157336),
(5415, 'https://www.youtube.com/embed/zSWdZVtXT7E', 157336),
(5416, 'https://www.youtube.com/embed/KlyknsTJk0w', 157336),
(5417, 'https://www.youtube.com/embed/0vxOhd4qlnA', 157336),
(5418, 'https://www.youtube.com/embed/827FNDpQWrQ', 157336),
(5419, 'https://www.youtube.com/embed/LY19rHKAaAg', 157336),
(5420, 'https://www.youtube.com/embed/QCSPda7xQ3s', 62127),
(5421, 'https://www.youtube.com/embed/xInh3VhAWs8', 102899),
(5422, 'https://www.youtube.com/embed/bGt-saFvkNk', 24428),
(5423, 'https://www.youtube.com/embed/hIR8Ar-Z4hw', 24428),
(5424, 'https://www.youtube.com/embed/eOrNdBpGMv8', 24428),
(5425, 'https://www.youtube.com/embed/2LqzF5WauAw', 157336),
(5426, 'https://www.youtube.com/embed/Rt2LHkSwdPQ', 157336),
(5427, 'https://www.youtube.com/embed/zClabIdSmhw', 2734),
(5428, 'https://www.youtube.com/embed/Dre0wQmLGe8', 69478),
(5429, 'https://www.youtube.com/embed/9XmFTADupMc', 69050),
(5430, 'https://www.youtube.com/embed/bxEK3mpvQes', 1416),
(5431, 'https://www.youtube.com/embed/f9OKL5no-S0', 62127),
(5432, 'https://www.youtube.com/embed/Dn49QoKGKr8', 62127),
(5433, 'https://www.youtube.com/embed/dW1BIid8Osg', 283995),
(5434, 'https://www.youtube.com/embed/wUn05hdkhjM', 283995),
(5435, 'https://www.youtube.com/embed/CaLNiC-bKHQ', 283995),
(5436, 'https://www.youtube.com/embed/1Q8fG0TtVAY', 297762),
(5437, 'https://www.youtube.com/embed/5lGoQhFb4NM', 297762),
(5438, 'https://www.youtube.com/embed/UEP1Mk6Un98', 281338),
(5439, 'https://www.youtube.com/embed/haXvp8M9Cog', 335984),
(5440, 'https://www.youtube.com/embed/INLzqh7rZ-U', 297762),
(5441, 'https://www.youtube.com/embed/sEizvn8-3Rw', 1403),
(5442, 'https://www.youtube.com/embed/RH3OxVFvTeg', 263115),
(5443, 'https://www.youtube.com/embed/XaE_9pfybL4', 263115),
(5444, 'https://www.youtube.com/embed/KIpGKumxiGg', 381288),
(5445, 'https://www.youtube.com/embed/6bfooooTKyM', 381288),
(5446, 'https://www.youtube.com/embed/Qsr6SgcKNiM', 381288),
(5447, 'https://www.youtube.com/embed/HHnKnVUySJU', 381288),
(5448, 'https://www.youtube.com/embed/JDcAlo8i2y8', 281338),
(5449, 'https://www.youtube.com/embed/BSXAOywn_rE', 283995),
(5450, 'https://www.youtube.com/embed/lVAwc1Ll7t4', 347882),
(5451, 'https://www.youtube.com/embed/pWdKf3MneyI', 102899),
(5452, 'https://www.youtube.com/embed/cx3joJnXydc', 102899),
(5453, 'https://www.youtube.com/embed/fis-9Zqu2Ro', 209112),
(5454, 'https://www.youtube.com/embed/NhWg7AQLI_8', 209112),
(5455, 'https://www.youtube.com/embed/0WWzgGyAH6Y', 209112),
(5456, 'https://www.youtube.com/embed/JwMKRevYa_M', 337339),
(5457, 'https://www.youtube.com/embed/-9FU7Ia-BKg', 337339),
(5458, 'https://www.youtube.com/embed/jeKBMdYaM3U', 337339),
(5459, 'https://www.youtube.com/embed/naQr0uTrH_s', 22),
(5460, 'https://www.youtube.com/embed/PbdM1db3JbY', 671),
(5461, 'https://www.youtube.com/embed/2WhQcK-Zaok', 283995),
(5462, 'https://www.youtube.com/embed/eX0KjDFvDAY', 283995),
(5463, 'https://www.youtube.com/embed/MTrLaWIcdAs', 283995),
(5464, 'https://www.youtube.com/embed/0LWEYReXjQs', 283995),
(5465, 'https://www.youtube.com/embed/Div0iP65aZo', 263115),
(5466, 'https://www.youtube.com/embed/PJTonrzXTJs', 69478),
(5467, 'https://www.youtube.com/embed/336qJITnDi0', 157350),
(5468, 'https://www.youtube.com/embed/sutgWjz10sM', 157350),
(5469, 'https://www.youtube.com/embed/nD4KuAWLJ1U', 157350),
(5470, 'https://www.youtube.com/embed/HUCBxfHjayo', 297762),
(5471, 'https://www.youtube.com/embed/tnbDVsL_JpQ', 297762),
(5472, 'https://www.youtube.com/embed/tsJeu11shJg', 297762),
(5473, 'https://www.youtube.com/embed/e9waCtSVoZ0', 297762),
(5474, 'https://www.youtube.com/embed/JUgbntoMyW8', 335984),
(5475, 'https://www.youtube.com/embed/qxjPjPzQ1iU', 281338),
(5476, 'https://www.youtube.com/embed/dgxBI_X1vGY', 281338),
(5477, 'https://www.youtube.com/embed/PepRo66Ru0s', 31917),
(5478, 'https://www.youtube.com/embed/EuOlYPSEzSc', 274855),
(5479, 'https://www.youtube.com/embed/Qz8cjvKJLuw', 274855),
(5480, 'https://www.youtube.com/embed/lI_Yau69onQ', 274855),
(5481, 'https://www.youtube.com/embed/V-CIpujhDXc', 274855),
(5482, 'https://www.youtube.com/embed/VnmPUW1lAKw', 274855),
(5483, 'https://www.youtube.com/embed/2Pypma3z1oI', 70513),
(5484, 'https://www.youtube.com/embed/_GWtgdAYHMU', 18165),
(5485, 'https://www.youtube.com/embed/gCcx85zbxz4', 335984),
(5486, 'https://www.youtube.com/embed/wWNk3COVNS4', 281338),
(5487, 'https://www.youtube.com/embed/yDBFrQvfGlQ', 281338),
(5488, 'https://www.youtube.com/embed/X6x-QSinrUU', 281338),
(5489, 'https://www.youtube.com/embed/h5XQq1ulspc', 281338),
(5490, 'https://www.youtube.com/embed/FCy9A-nfj24', 281338),
(5491, 'https://www.youtube.com/embed/vm7vE9ujNYs', 281338),
(5492, 'https://www.youtube.com/embed/ThXG-Kq-RkQ', 281338),
(5493, 'https://www.youtube.com/embed/NeZ8BQBl8NA', 281338),
(5494, 'https://www.youtube.com/embed/f_Q-R2gICco', 281338),
(5495, 'https://www.youtube.com/embed/hHUBpMznFJI', 281338),
(5496, 'https://www.youtube.com/embed/1TxqmlIv1Iw', 281338),
(5497, 'https://www.youtube.com/embed/unciPjkH-O4', 263115),
(5498, 'https://www.youtube.com/embed/Am-gG-VyXPg', 268896),
(5499, 'https://www.youtube.com/embed/ye6GCY_vqYk', 181808),
(5500, 'https://www.youtube.com/embed/zB4I68XVPzQ', 181808),
(5501, 'https://www.youtube.com/embed/GQQMLE4FuIQ', 181808),
(5502, 'https://www.youtube.com/embed/407GVB88b60', 67744),
(5503, 'https://www.youtube.com/embed/idZPTuwcMJA', 406990),
(5505, 'https://www.youtube.com/embed/3cxixDgHUYw', 141052),
(5506, 'https://www.youtube.com/embed/gglkYMGRYlE', 141052),
(5507, 'https://www.youtube.com/embed/VSB4wGIdDwo', 297762),
(5508, 'https://www.youtube.com/embed/2Q18TnxZxLI', 37680),
(5509, 'https://www.youtube.com/embed/idiNjlYU7Cs', 1911),
(5510, 'https://www.youtube.com/embed/yy96yJjkvjo', 1622),
(5511, 'https://www.youtube.com/embed/8ndhidEmUbI', 338970),
(5512, 'https://www.youtube.com/embed/Vu4UPet8Nyc', 396371),
(5513, 'https://www.youtube.com/embed/Fgo6xs93OEw', 470878),
(5514, 'https://www.youtube.com/embed/fUjicxMPDzs', 268896),
(5515, 'https://www.youtube.com/embed/r9-DM9uBtVI', 141052),
(5516, 'https://www.youtube.com/embed/ue-RthZ6WWk', 671),
(5517, 'https://www.youtube.com/embed/Q61YhARNOPg', 671),
(5518, 'https://www.youtube.com/embed/k71hjl3zWsA', 671),
(5519, 'https://www.youtube.com/embed/Q0CbN8sfihY', 181808),
(5521, 'https://www.youtube.com/embed/WcIfbdfZRDQ', 181808),
(5522, 'https://www.youtube.com/embed/1Mlhnt0jMlg', 1399),
(5523, 'https://www.youtube.com/embed/giYeaKsXnsI', 1399),
(5524, 'https://www.youtube.com/embed/yu8eRaq1FUM', 1399),
(5525, 'https://www.youtube.com/embed/WBb3fojgW0Q', 1418),
(5526, 'https://www.youtube.com/embed/AvKARpGWztE', 375098),
(5527, 'https://www.youtube.com/embed/9v-pw0ZQRLY', 396371),
(5528, 'https://www.youtube.com/embed/ipVHY_UZ6zM', 396371),
(5529, 'https://www.youtube.com/embed/JctIuZfSsa4', 63247),
(5530, 'https://www.youtube.com/embed/I5mUUOZT6b4', 1418),
(5531, 'https://www.youtube.com/embed/ThSi1NL2DUU', 37680),
(5532, 'https://www.youtube.com/embed/uIyUWdqLnGc', 335984),
(5533, 'https://www.youtube.com/embed/OkykXOhytwo', 2691),
(5534, 'https://www.youtube.com/embed/p9wE8dyzEJE', 447332),
(5535, 'https://www.youtube.com/embed/coOKvrsmQiI', 427641),
(5536, 'https://www.youtube.com/embed/vS0Gw7oVsYc', 396371),
(5537, 'https://www.youtube.com/embed/-py2awmME8s', 181808),
(5538, 'https://www.youtube.com/embed/zIauywtvcjI', 181808),
(5539, 'https://www.youtube.com/embed/XcSMdhfKga4', 458423),
(5540, 'https://www.youtube.com/embed/h1-AXcb5j18', 458423),
(5541, 'https://www.youtube.com/embed/MPuXL8z_e0w', 141052),
(5542, 'https://www.youtube.com/embed/pbnSaCTtZ2Q', 141052),
(5543, 'https://www.youtube.com/embed/sLobJuwyRtM', 141052),
(5544, 'https://www.youtube.com/embed/EFVZConRIZ0', 141052),
(5545, 'https://www.youtube.com/embed/kLkiEdGqiKE', 141052),
(5546, 'https://www.youtube.com/embed/OLAJ0ooorDU', 454619),
(5547, 'https://www.youtube.com/embed/MLn1IuAyKaI', 57243),
(5548, 'https://www.youtube.com/embed/RYIu7Qlqh4M', 57243),
(5549, 'https://www.youtube.com/embed/XHk5kCIiGoM', 11),
(5550, 'https://www.youtube.com/embed/QRtBP07gIHY', 400579),
(5551, 'https://www.youtube.com/embed/OsMyv9Q4_OU', 470878),
(5552, 'https://www.youtube.com/embed/xxQhWrAcQnE', 69478),
(5553, 'https://www.youtube.com/embed/UgsS3nhRRzQ', 335984),
(5554, 'https://www.youtube.com/embed/BBsjZgu7T2U', 335984),
(5555, 'https://www.youtube.com/embed/BFPb308r6u4', 335984),
(5556, 'https://www.youtube.com/embed/nfIrTxSONu4', 338970),
(5557, 'https://www.youtube.com/embed/x6H7k3XBlk4', 1418),
(5558, 'https://www.youtube.com/embed/e0p04CLd0gk', 1418),
(5559, 'https://www.youtube.com/embed/1Zkb8Cn-N28', 1418),
(5560, 'https://www.youtube.com/embed/xNZx5aB11aI', 1418),
(5561, 'https://www.youtube.com/embed/Iv42ZFZubS0', 1418),
(5562, 'https://www.youtube.com/embed/FpGkLzGl1CI', 1418),
(5563, 'https://www.youtube.com/embed/38x6kWB-xD4', 1418),
(5564, 'https://www.youtube.com/embed/yHTEOntn1i0', 1418),
(5565, 'https://www.youtube.com/embed/tQKYb9m1ZN4', 1418),
(5566, 'https://www.youtube.com/embed/yngj_U99pB4', 1418),
(5567, 'https://www.youtube.com/embed/4-BTxXm8KSg', 336843),
(5568, 'https://www.youtube.com/embed/xjZQk3hZV3Y', 336843),
(5569, 'https://www.youtube.com/embed/S_9OSktlm6s', 336843),
(5570, 'https://www.youtube.com/embed/Ww1GiTIJF_Y', 336843),
(5571, 'https://www.youtube.com/embed/Cn7zS77xU9w', 336843),
(5572, 'https://www.youtube.com/embed/r708nUtriNQ', 336843),
(5573, 'https://www.youtube.com/embed/VGDm53oBW_0', 336843),
(5574, 'https://www.youtube.com/embed/psS35J_qlrE', 336843),
(5575, 'https://www.youtube.com/embed/sP6Z94dDPEw', 336843),
(5576, 'https://www.youtube.com/embed/lATFAVrTFKQ', 336843),
(5577, 'https://www.youtube.com/embed/fjrbOa5aCCo', 336843),
(5578, 'https://www.youtube.com/embed/P8Q6ma2sfJQ', 447332),
(5579, 'https://www.youtube.com/embed/8BAhwgjMvnM', 268896),
(5580, 'https://www.youtube.com/embed/dDISAQMhHSE', 268896),
(5581, 'https://www.youtube.com/embed/yR3tiOwq138', 268896),
(5582, 'https://www.youtube.com/embed/3KkhD0MnaJU', 338970),
(5583, 'https://www.youtube.com/embed/oWXq7mKcx5s', 338970),
(5584, 'https://www.youtube.com/embed/o0USj9KfYZc', 429417),
(5585, 'https://www.youtube.com/embed/ZZuLKf4XYDY', 429417),
(5586, 'https://www.youtube.com/embed/57uWyhnbsd8', 429417),
(5587, 'https://www.youtube.com/embed/oOoZgb7mycg', 429417),
(5588, 'https://www.youtube.com/embed/MnKbM9Zxtn8', 340022),
(5589, 'https://www.youtube.com/embed/QYuo8xy_XwU', 340022),
(5590, 'https://www.youtube.com/embed/Sd6B_wNrA3w', 340022),
(5591, 'https://www.youtube.com/embed/I622xI7N0uc', 340022),
(5592, 'https://www.youtube.com/embed/aopextscHWQ', 493006),
(5593, 'https://www.youtube.com/embed/xjDjIWPwcPU', 284054),
(5594, 'https://www.youtube.com/embed/PmUL6wMpMWw', 401981),
(5595, 'https://www.youtube.com/embed/4WbsAAQknn0', 401981),
(5596, 'https://www.youtube.com/embed/fmzxYb5eEu0', 401981),
(5597, 'https://www.youtube.com/embed/rLR-YO_Yuag', 401981),
(5598, 'https://www.youtube.com/embed/wOPJ60X7eZU', 401981),
(5599, 'https://www.youtube.com/embed/by6RjitIG5c', 401981),
(5600, 'https://www.youtube.com/embed/aGvZX19LvWo', 401981),
(5601, 'https://www.youtube.com/embed/ckJU4s5LT70', 401981),
(5602, 'https://www.youtube.com/embed/6ofUJWUrt0A', 401981),
(5603, 'https://www.youtube.com/embed/AykpBMS4Kqk', 401981),
(5604, 'https://www.youtube.com/embed/M9VqmPX8m2k', 401981),
(5605, 'https://www.youtube.com/embed/ZQUBjoGm1ls', 401981),
(5607, 'https://www.youtube.com/embed/HnnRyhuX_jM', 284054),
(5608, 'https://www.youtube.com/embed/jJm2WFTscDM', 284054),
(5609, 'https://www.youtube.com/embed/y9fBzUtrKwY', 284054),
(5610, 'https://www.youtube.com/embed/yH72D12BkzY', 284054),
(5611, 'https://www.youtube.com/embed/wwoq3o_yNMw', 1434),
(5612, 'https://www.youtube.com/embed/XJAjw42JjZs', 4057),
(5613, 'https://www.youtube.com/embed/-43MBOJnVks', 427641),
(5614, 'https://www.youtube.com/embed/e5D3O4yCmCg', 400579),
(5615, 'https://www.youtube.com/embed/cQYvQIrM1FY', 71446),
(5616, 'https://www.youtube.com/embed/g_6yBZKj-eo', 141052),
(5617, 'https://www.youtube.com/embed/uUdW72P1POQ', 470878),
(5618, 'https://www.youtube.com/embed/HyNJ3UrGk_I', 345887),
(5619, 'https://www.youtube.com/embed/vONLB2l3ZrM', 345887),
(5620, 'https://www.youtube.com/embed/dZOaI_Fn5o4', 335984),
(5621, 'https://www.youtube.com/embed/ePbKGoIGAXY', 157336),
(5622, 'https://www.youtube.com/embed/p8rU4ysw-5M', 1668),
(5623, 'https://www.youtube.com/embed/TgP8v60X23c', 1668),
(5624, 'https://www.youtube.com/embed/ZAXA1DV4dtI', 71446),
(5632, 'https://www.youtube.com/embed/ORL1d7GWoBc', 347882),
(5633, 'https://www.youtube.com/embed/xDcxNSxWDxg', 347882),
(5634, 'https://www.youtube.com/embed/eXncAoGeDNk', 347882),
(5635, 'https://www.youtube.com/embed/Th0Tlj8XL1A', 336843),
(5636, 'https://www.youtube.com/embed/xCSontBpl5k', 336843),
(5637, 'https://www.youtube.com/embed/AEWvRqZQ0RU', 476292),
(5638, 'https://www.youtube.com/embed/xf_RtB6oBvA', 454983),
(5639, 'https://www.youtube.com/embed/DAi3GgwhDR0', 181808),
(5640, 'https://www.youtube.com/embed/_5wbBkDeshc', 181808),
(5641, 'https://www.youtube.com/embed/BQRZ0ODUUzQ', 181808),
(5642, 'https://www.youtube.com/embed/KfTalQLQi3o', 181808),
(5643, 'https://www.youtube.com/embed/L376bLr_xso', 181808),
(5644, 'https://www.youtube.com/embed/oao0bFkV4sA', 181808),
(5645, 'https://www.youtube.com/embed/SFxZb-GHbrQ', 181808),
(5646, 'https://www.youtube.com/embed/SJNfysZRmf4', 181808),
(5647, 'https://www.youtube.com/embed/\"-2Wy_dIrntk\"', 400579),
(5648, 'https://www.youtube.com/embed/\"PJBmeqpw3DY\"', 284054),
(5649, 'https://www.youtube.com/embed/\"sgX_StgXGF0\"', 284054),
(5650, 'https://www.youtube.com/embed/\"zkWiTLKE0mg\"', 284054),
(5651, 'https://www.youtube.com/embed/\"2GTI0MHj3dM\"', 284054),
(5652, 'https://www.youtube.com/embed/\"3nS_wzicYX4\"', 284054),
(5653, 'https://www.youtube.com/embed/\"mSAgLsrZG2Q\"', 284054),
(5654, 'https://www.youtube.com/embed/\"u0SBlLWNe5s\"', 284054),
(5655, 'https://www.youtube.com/embed/\"LBmD1UwLLw0\"', 399055),
(5656, 'https://www.youtube.com/embed/\"368_ovPsJk8\"', 399055),
(5657, 'https://www.youtube.com/embed/\"XFYWazblaUA\"', 399055),
(5658, 'https://www.youtube.com/embed/\"WR7cc5t7tv8\"', 447332),
(5659, 'https://www.youtube.com/embed/\"I5R8HpxGYO4\"', 458423),
(5660, 'https://www.youtube.com/embed/\"r2TQAwcLJJg\"', 458423),
(5661, 'https://www.youtube.com/embed/\"dxWvtMOGAhw\"', 284054),
(5662, 'https://www.youtube.com/embed/\"E7qxz8sRLB0\"', 345887),
(5663, 'https://www.youtube.com/embed/\"t9QePUT-Yt8\"', 447200),
(5664, 'https://www.youtube.com/embed/\"_pIEzZVqwFs\"', 447200),
(5665, 'https://www.youtube.com/embed/\"-nEB0oCwgxI\"', 427641),
(5666, 'https://www.youtube.com/embed/Mh8MYFadTmQ', 62688),
(5667, 'https://www.youtube.com/embed/R6YnLV8k3_w', 39272),
(5668, 'https://www.youtube.com/embed/\"_V-5kmDYh9Y\"', 345887),
(5669, 'https://www.youtube.com/embed/6lKZYG5IgKI', 70513),
(5670, 'https://www.youtube.com/embed/\"Ku52zNnft8k\"', 400155),
(5671, 'https://www.youtube.com/embed/\"d5exSS74Lh0\"', 400155),
(5672, 'https://www.youtube.com/embed/\"Iy3kdlgw2Sg\"', 400155),
(5673, 'https://www.youtube.com/embed/\"AaClwCvsmxM\"', 400155),
(5674, 'https://www.youtube.com/embed/\"H0IpL1W4W4k\"', 492719),
(5675, 'https://www.youtube.com/embed/AZ65JKBLoq8', 60735),
(5676, 'https://www.youtube.com/embed/7XEQqaV_dTM', 46896),
(5677, 'https://www.youtube.com/embed/x6QKyx7GP1A', 1433),
(5678, 'https://www.youtube.com/embed/SmtFgAKXx28', 1433),
(5679, 'https://www.youtube.com/embed/W9Kf1RUHLxE', 1433),
(5680, 'https://www.youtube.com/embed/yQCVkGwasDg', 1433),
(5681, 'https://www.youtube.com/embed/7hn-Cs_6QL4', 1433),
(5682, 'https://www.youtube.com/embed/sJMN3CtVz-0', 1433),
(5683, 'https://www.youtube.com/embed/bjqEWgDVPe0', 1399),
(5684, 'https://www.youtube.com/embed/XuKfFzk1uQs', 1399),
(5685, 'https://www.youtube.com/embed/nblUgAMoOvU', 1399),
(5686, 'https://www.youtube.com/embed/aj9x_Xea6cg', 1399),
(5687, 'https://www.youtube.com/embed/Pu5FAWnFRI0', 62127),
(5692, 'https://www.youtube.com/embed/lD41XdWcmbY', 493007),
(5693, 'https://www.youtube.com/embed/VHW0RkFStng', 1622),
(5694, 'https://www.youtube.com/embed/Nt9L1jCKGnE', 493008);

-- --------------------------------------------------------

--
-- Table structure for table `tv_series`
--

CREATE TABLE `tv_series` (
  `noOfSeasons` tinyint(3) UNSIGNED NOT NULL,
  `id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profilePic` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `isAdmin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `profilePic`, `isAdmin`) VALUES
(16, 'Ghayas', 'ghayasbaig247@gmail.com', NULL, '$2y$10$rf9X/xrKIOetv7vIeYhEhuT0Fvo82r0EcuVWymI4JVj4u0ADfJmKG', 'm3FNCUQHUy8tY4t6xuytWNvxK47UsSY3oqjSG2QRIDpjuY3ESUyT3uaaZlMw', '2019-02-23 08:49:35', '2019-02-24 07:29:31', 'pps/16/profilePic.jpg', 1),
(17, 'MGB', 'mgb@gmail.com', NULL, '$2y$10$EOOYb98VjwGSukyUSKDWTONF0SK9XVOh/kzp1InduJCix9rGNugAm', NULL, '2019-02-23 09:53:53', '2019-02-24 06:57:22', 'pps/17/profilePic.jpg', 0),
(18, 'Voldemort', 'voldemort@example.com', NULL, '$2y$10$JPSRWUOchIIZkLzndhYyYO1/iIi4W/QbH5F/2kyorf5ntiwJsE16K', NULL, '2019-02-24 08:03:00', '2019-02-24 08:45:12', 'pps/18/profilePic.jpg', 0),
(19, 'Harry', 'potter@gmail.com', NULL, '$2y$10$57qRZP2ASDDbau7jC4ZXwedmnYPzg8XlVwoh.z04jShTksYbY9cfC', NULL, '2019-02-24 09:24:21', '2019-03-08 06:50:54', 'pps/19/profilePic.jpg', 0),
(20, 'Hey', 'hey@gmail.com', NULL, '$2y$10$Y2zvzb.CIQqFyzNJKccmieUwDUEXjyj2nzf0pcKIYbUkz59542S4q', NULL, '2019-02-24 10:16:17', '2019-02-24 10:33:37', 'pps/20/profilePic.jpg', 0),
(21, 'Bam', 'bam@gmail.com', NULL, '$2y$10$Jx.ky0K59SHzeiGn9ieiQeC9IEAhOKPqDZEVn29ylzWd8gLEIsj5q', NULL, '2019-02-24 10:58:26', '2019-02-24 11:52:27', 'pps/21/profilePic.jpg', 0),
(22, 'Art', 'art@gmail.com', NULL, '$2y$10$7uB2HiYs1X33XjHrmVnoI.hDoQso6S/p6w32mOf74uwbggDGJSxp6', NULL, '2019-02-27 07:21:43', '2019-02-27 08:39:29', 'pps/22/profilePic.jpg', 0),
(23, 'Apk', 'apk@gmail.com', NULL, '$2y$10$TF5CEaGQR/mP8Gv5b/VoKO3PR1sajhRFfnQauoQC8aX6kowjR4YV2', NULL, '2019-02-27 10:39:49', '2019-02-27 10:40:18', 'pps/23/profilePic.jpg', 0),
(24, 'TrancePoze', 'trancepoze@gmail.com', NULL, '$2y$10$a9SbMAoDU9LlhCxd2DZ3YOJSGLePJWXK9qj7Tv6I1fERUcom5hVpS', NULL, '2019-03-21 11:58:00', '2019-03-21 11:58:00', '', 0),
(25, 'Harry', 'harry@gmail.com', NULL, '$2y$10$sKetxknO.0TmKahSwR6dBepHnLOkmzN1ParKhI2Jqv5n7.OX7aURm', 'OevhpPbVHOcZXX0ZEyCV16WTtPROpvE8R0jYKDP3lmK6zoDfFNBTuFNwEkdN', '2019-06-26 10:35:38', '2019-06-26 10:35:38', '', 0),
(26, 'John Wick', 'johnwick@gmail.com', NULL, '$2y$10$tm1WQJmjMMtlU4Fze.y/3ebw38CUYNUKH3uzbQw9W5t4DpQeA7cLK', 'Amh4UrMocPiQ8u2q5ba8MzqbDkzH1mRC2buGfGf9Rj0Vm2XbB0H49yVbWtEK', '2019-07-06 10:37:00', '2019-07-06 10:37:00', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_carts`
--

CREATE TABLE `user_carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `movie_id` int(10) UNSIGNED NOT NULL,
  `quality_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_carts`
--

INSERT INTO `user_carts` (`id`, `user_id`, `movie_id`, `quality_id`, `created_at`, `updated_at`) VALUES
(42, 16, 493007, 2, '2019-07-08 12:50:49', '2019-07-08 12:50:49'),
(45, 16, 400155, 3, '2019-07-08 12:51:55', '2019-07-08 12:51:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`actor_movies_id`);

--
-- Indexes for table `actor_movies`
--
ALTER TABLE `actor_movies`
  ADD KEY `actor_movies_actorid_foreign` (`id`),
  ADD KEY `actor_movies_movieid_foreign` (`movie_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_userid_foreign` (`user_id`),
  ADD KEY `comments_movieid_foreign` (`movie_id`);

--
-- Indexes for table `episodes`
--
ALTER TABLE `episodes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `episodes_seriesid_foreign` (`seriesID`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD KEY `favorites_user_foreign` (`user`),
  ADD KEY `favorites_movie_foreign` (`movie`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`movie_genres_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD KEY `movie_genres_genreid_foreign` (`id`),
  ADD KEY `movie_genres_movieid_foreign` (`movie_id`);

--
-- Indexes for table `movie_qualities`
--
ALTER TABLE `movie_qualities`
  ADD KEY `movie_qualities_movie_id_foreign` (`movie_id`),
  ADD KEY `movie_qualities_quality_id_foreign` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_status_foreign` (`status`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_movie_id_foreign` (`movie_id`),
  ADD KEY `order_details_quality_id_foreign` (`quality_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `qualities`
--
ALTER TABLE `qualities`
  ADD PRIMARY KEY (`movie_quality_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD KEY `ratings_userid_foreign` (`userID`),
  ADD KEY `ratings_movieid_foreign` (`movieID`);

--
-- Indexes for table `trailers`
--
ALTER TABLE `trailers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trailers_movieid_foreign` (`movie_id`);

--
-- Indexes for table `tv_series`
--
ALTER TABLE `tv_series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_carts`
--
ALTER TABLE `user_carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_carts_user_id_foreign` (`user_id`),
  ADD KEY `user_carts_movie_id_foreign` (`movie_id`),
  ADD KEY `user_carts_quality_id_foreign` (`quality_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors`
--
ALTER TABLE `actors`
  MODIFY `actor_movies_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2034429;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `episodes`
--
ALTER TABLE `episodes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `movie_genres_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10773;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=493009;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `qualities`
--
ALTER TABLE `qualities`
  MODIFY `movie_quality_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trailers`
--
ALTER TABLE `trailers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5695;

--
-- AUTO_INCREMENT for table `tv_series`
--
ALTER TABLE `tv_series`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user_carts`
--
ALTER TABLE `user_carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actor_movies`
--
ALTER TABLE `actor_movies`
  ADD CONSTRAINT `actor_movies_actorid_foreign` FOREIGN KEY (`id`) REFERENCES `actors` (`actor_movies_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `actor_movies_movieid_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_movieid_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_userid_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `episodes`
--
ALTER TABLE `episodes`
  ADD CONSTRAINT `episodes_seriesid_foreign` FOREIGN KEY (`seriesID`) REFERENCES `tv_series` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_movie_foreign` FOREIGN KEY (`movie`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favorites_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `movie_genres`
--
ALTER TABLE `movie_genres`
  ADD CONSTRAINT `movie_genres_genreid_foreign` FOREIGN KEY (`id`) REFERENCES `genres` (`movie_genres_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `movie_genres_movieid_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `movie_qualities`
--
ALTER TABLE `movie_qualities`
  ADD CONSTRAINT `movie_qualities_movie_id_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `movie_qualities_quality_id_foreign` FOREIGN KEY (`id`) REFERENCES `qualities` (`movie_quality_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_status_foreign` FOREIGN KEY (`status`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_movie_id_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_details_quality_id_foreign` FOREIGN KEY (`quality_id`) REFERENCES `qualities` (`movie_quality_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_movieid_foreign` FOREIGN KEY (`movieID`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ratings_userid_foreign` FOREIGN KEY (`userID`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trailers`
--
ALTER TABLE `trailers`
  ADD CONSTRAINT `trailers_movieid_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tv_series`
--
ALTER TABLE `tv_series`
  ADD CONSTRAINT `tv_series_id_foreign` FOREIGN KEY (`id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_carts`
--
ALTER TABLE `user_carts`
  ADD CONSTRAINT `user_carts_movie_id_foreign` FOREIGN KEY (`movie_id`) REFERENCES `movies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_carts_quality_id_foreign` FOREIGN KEY (`quality_id`) REFERENCES `qualities` (`movie_quality_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
